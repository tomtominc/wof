﻿
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
///     The pivot represents the middle value to which all weights are adjusted if a bonus is applied. Weights are left
///     unadjusted if no bonus is applied to the selection.
/// </summary>
public enum SelectionPivot
{
    Median,
    Mean
}

/// <summary>
///     The range represents the subset of items that will have a bonus applied, if one is specified in the selection.
///     "BelowPivot" refers to all items with weights less than the pivot, "AbovePivot" refers to all the items with
///     weights greater than the pivot, and "All" adjusts to all weights, regardless of their relationship to the pivot
///     value. Due to this, "All" has a more pronounced "sweetening" effect than "BelowPivot" or "AbovePivot".
/// </summary>
public enum SelectionRange
{
    BelowPivot,
    AbovePivot,
    All
}

/// <summary>
///     This class must be instantiated to execute weighted selections. It can select over any collection of objects as
///     long as the object implements ISelectable and the collection implements IEnumerable.
/// </summary>
public partial class Selector
{
    private readonly SelectionRange _bonusMode;
    private readonly SelectionPivot _selectionPivot;

    /// <summary>
    ///     Creates a new selector (the object that makes the weighted selections) with the specified pivot, that operates over
    ///     the specified range.
    /// </summary>
    /// <param name="selectionPivot">
    ///     The pivot is the value to which the weights are adjusted, if a bonus is applied.
    ///     For example, SelectionPivot.Median would increase weights below the median value for all weights,
    ///     and decrease weights above the median value
    /// </param>
    /// <param name="selectionRange">
    ///     The range is the subset of all weights that will be adjusted towards the pivot.
    ///     Below (less than the pivot), Above (greater than the pivot), or All weights.
    /// </param>
    public Selector(SelectionPivot selectionPivot, SelectionRange selectionRange)
    {
        _selectionPivot = selectionPivot;
        _bonusMode = selectionRange;
    }

    /// <summary>
    ///     Creates a default selector (the object that makes the weighted selections) that pushes all weights towards their
    ///     average, if a bonus is applied.
    /// </summary>
    public Selector()
    {
        _selectionPivot = SelectionPivot.Mean;
        _bonusMode = SelectionRange.All;
    }

    /// <summary>
    ///     Selects a single item, based on weight.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="candidates">An enumerable collection of objects that implement ISelectable</param>
    /// <returns>One single object from the set, selected by weight</returns>
    public T Single<T>(IEnumerable<T> candidates) where T : ISelectable
    {
        return Single(candidates, 0);
    }

    /// <summary>
    ///     Selects a single item, based on weight, adjusted by a bonus value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="candidates">An enumerable collection of objects that implement ISelectable</param>
    /// <param name="bonus">
    ///     The degree to which values are pushed towards the selector's pivot. Higher bonus values drive the
    ///     weights closer to an unweighted selection.
    /// </param>
    /// <returns>One single object from the set, selected by post-sweetened weight</returns>
    public T Single<T>(IEnumerable<T> candidates, float bonus) where T : ISelectable
    {
        T[] enumerable = candidates as T[] ?? candidates.ToArray();
        if (!enumerable.Any())
            throw new Exception("Cannot select from an empty list");
        if (!(bonus > 0))
            return Get(enumerable);

        float sum = enumerable.Sum(s => s.GetSelectionWeight());

        IEnumerable<BonusContainer<T>> adjustedCandidates =
            enumerable.Select(c => new BonusContainer<T>(c, c.GetSelectionWeight() / sum));

        adjustedCandidates = ApplyAdjust(adjustedCandidates, bonus);

        return Get(adjustedCandidates).ObjectRef;
    }

    /// <summary>
    ///     Selects a number of items, based on weight.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="candidates">An enumerable collection of objects that implement ISelectable</param>
    /// <param name="quantity">The number of selectable objects to return</param>
    /// <returns>An enumerable collection of objects, selected by weight, of the specified length</returns>
    public IEnumerable<T> Multiple<T>(IEnumerable<T> candidates, int quantity) where T : ISelectable
    {
        return Multiple(candidates, quantity, 0);
    }

    /// <summary>
    ///     Selects a number of items, based on weight, adjusted by a bonus value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="candidates">An enumerable collection of objects that implement ISelectable</param>
    /// <param name="quantity">The number of selectable objects to return</param>
    /// <param name="bonus">
    ///     The degree to which values are pushed towards the selector's pivot. Higher bonus values drive the
    ///     weights closer to an unweighted selection.
    /// </param>
    /// <returns>An enumerable collection of objects, selected by weight, of the specified length</returns>
    public T[] Multiple<T>(IEnumerable<T> candidates, int quantity, float bonus) where T : ISelectable
    {
        T[] enumerable = candidates as T[] ?? candidates.ToArray();
        if (!enumerable.Any())
            throw new Exception("Cannot select from an empty list");
        if (!(bonus > 0))
            return Get(enumerable, quantity);

        float sum = enumerable.Sum(s => s.GetSelectionWeight());

        IEnumerable<BonusContainer<T>> adjustedCandidates =
            enumerable.Select(c => new BonusContainer<T>(c, c.GetSelectionWeight() / sum));

        adjustedCandidates = ApplyAdjust(adjustedCandidates, bonus);

        return Get(adjustedCandidates, quantity).Select(s => s.ObjectRef).ToArray();
    }

    /// <summary>
    ///     Returns a dictionary, keyed by selectable objects with values of their selection weights, adjusted by a bonus
    ///     value.
    /// </summary>
    /// <param name="candidates">An enumerable collection of objects that implement ISelectable</param>
    /// <param name="bonus">
    ///     The degree to which values are pushed towards the selector's pivot. Higher bonus values drive the
    ///     weights closer to an unweighted selection.
    /// </param>
    /// <returns>
    ///     Returns a dictionary, keyed by selectable objects with values of their selection weights, adjusted by a bonus
    ///     value.
    /// </returns>
    public Dictionary<T, float> GetWeightDictionary<T>(IEnumerable<T> candidates, float bonus) where T : ISelectable
    {
        T[] enumerable = candidates as T[] ?? candidates.ToArray();
        if (!enumerable.Any())
            return default(Dictionary<T, float>);
        if (!(bonus > 0))
            return enumerable.ToDictionary(k => k, v => v.GetSelectionWeight());

        var adjustedCandidates = enumerable.Select(c =>
                new BonusContainer<T>(c, c.GetSelectionWeight() / enumerable.Sum(s => s.GetSelectionWeight()))); 
        adjustedCandidates = ApplyAdjust(adjustedCandidates, bonus);

        return adjustedCandidates.ToDictionary(k => k.ObjectRef, v => v.SelectionWeight);
    }
}
