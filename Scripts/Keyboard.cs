﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class Keyboard : MonoBehaviour
{ 
    public List<KeyButton> Keys = new List<KeyButton>();

    public static readonly int _buttonGuessIndex = 28;
    public static readonly int _buttonSubmitIndex = 27;
    private static readonly int _buttonBackspaceIndex = 19;
    private static readonly int[] _buttonRSTLNEIndex = {3,11,4,18,25,2};
    private static readonly  int[] _buttonVowelsIndex = {10,7,8,6};             // except E because E is in revealed character
    [SerializeField] private CanvasGroup _keyboardCanvasGroup;

    [SerializeField] private GameObject _disabledButtonPopUp;
    private GameObject _popUp;
    
    public List<KeyButton> PickedLetterKeys = new List<KeyButton>();

    public void SetUpCallBack()
    {
        for (var i = 0; i < Keys.Count; i++)
        {
            if (i == _buttonSubmitIndex || i == _buttonGuessIndex || i == _buttonBackspaceIndex) continue;

            // Debug.Log(k.Key);
            KeyButton temp = Keys[i];
            Keys[i].OnDisabledClickAction += delegate {

                // finished with bug : pop up shouldn't appear when disabling the button, it should be appear only when the button is alraedy disabled
                // The other button shouldnt be included (Guess) 
                if (_popUp != null)
                {

                    GameObject.Destroy(_popUp.gameObject);
                }

                _popUp = Instantiate(_disabledButtonPopUp, new Vector3(0, (_disabledButtonPopUp.GetComponent<RectTransform>().rect.height / 2), 0), Quaternion.identity) as GameObject;
                _popUp.transform.SetParent(temp.transform, false);
                _popUp.transform.SetParent(this.gameObject.transform, true);

                Debug.Log(temp.gameObject.GetComponent<RectTransform>().position);
            };
        }
    }

    // Use to disable keyboard
    public void disableKeyboard()
    {
        _keyboardCanvasGroup.interactable = false;
    }

    // Use to enable keyboard
    public void enableKeyboard()
    {
        _keyboardCanvasGroup.interactable = true;
    }

    // Use to disable all key button
    public void disableAllKeyboardButton()
    {
        for (int i = 0; i < Keys.Count; i++)
        {
            StartCoroutine(Keys[i].disableButton());
        }
    }

    // Use to enable all key button
    public void enableAllKeyboardButton()
    {
        // For every keys in keyboard
        for (int i = 0; i < Keys.Count; i++)
        {
            // enable key
            StartCoroutine(Keys[i].enableButton());
        }
    }

    // Use to enable the backspace button
    public void enableBackspace()
    {
        StartCoroutine(Keys[_buttonBackspaceIndex].enableButton());
    }

    // Use to disable the backspace key
    public void disableBackspace()
    {
        StartCoroutine(Keys[_buttonBackspaceIndex].disableButton());
    }

    // Use to enable the submit key
    public void enableSubmit()
    {
        StartCoroutine(Keys[_buttonSubmitIndex].enableButton());
    }

    // Use to disable the submit key
    public void disableSubmit()
    {
        StartCoroutine(Keys[_buttonSubmitIndex].disableButton());
    }

    // Use to show the submit key
    public void ShowSubmit()
    {
        Keys[_buttonSubmitIndex].gameObject.SetActive(true);
    }

    // Use to hide the submit key
    public void HideSubmit()
    {
        Keys[_buttonSubmitIndex].gameObject.SetActive(false);
    }
    
    public void disableGuess()
    {
        StartCoroutine(Keys[_buttonGuessIndex].disableButton());
    }
    
    public void enableGuess()
    {
        StartCoroutine(Keys[_buttonGuessIndex].enableButton());
    }

    // Use to hide the guess key
    public void HideGuess()
    {
        Keys[_buttonGuessIndex].gameObject.SetActive(false);
    }

    // Use to disable the revealed character key
    public void disableRSTLNEkey()
    {
        foreach (var index in _buttonRSTLNEIndex)
        {
            StartCoroutine(Keys[index].disableButton());
        }
    }

    // Use to disable the vowels key except the E
    public void disableVowels()
    {
        foreach (var index in _buttonVowelsIndex)
        {
            StartCoroutine(Keys[index].disableButton());
        }
    }

    // Use to enable the vowels key except the E
    public void enableVowels()
    {
        foreach (var index in _buttonVowelsIndex)
        {
            StartCoroutine(Keys[index].enableButton());
        }
    }

    public void DisablePickedLetters()
    {
        if (PickedLetterKeys != null)
        {
            foreach (KeyButton key in PickedLetterKeys) {
                StartCoroutine(key.disableButton());
            }
        }
    }

    public void EnablePickedLetters()
    {
        if (PickedLetterKeys != null)
        {
            foreach (KeyButton key in PickedLetterKeys)
            {
                StartCoroutine(key.enableButton());
            }
        }
    }
}
