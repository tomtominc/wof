﻿using UnityEngine;
using System.Collections;

public class CameraGUIGetter : MonoBehaviour
{
    public SortingLayer layer;

    private void Awake()
    {
        GetComponent < Canvas >().worldCamera = Camera.main;
        GetComponent < Canvas >().sortingLayerName = "Foreground";
    }
}
