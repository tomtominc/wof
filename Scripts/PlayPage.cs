﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using LitJson;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using DG.Tweening;

public class PlayPage : MonoBehaviour
{
    private const string _animatorOutStateName = "PageFadeOut";

    [SerializeField] private bool _cheatMode = false;
    [SerializeField] private Animation _pageAnimation;

    [SerializeField] private AudioClip _correctSound;
    [SerializeField] private AudioClip _wrongSound;
    [SerializeField] private AudioClip _timeUpSound;
    [SerializeField] private AudioClip _fireworksSound;
    [SerializeField] private AudioClip _crowdSound;
    [SerializeField] private AudioClip _gameStartSound;
    [SerializeField] private AudioClip _gameStartSound2;
    
    [SerializeField] private AudioSource _countdownSoundController;
    
    [SerializeField] private GameObject _playPage;
    [SerializeField] private GameObject _timerPrefab;
    [SerializeField] private GameObject _keyboardPrefab;
    [SerializeField] private GameObject _answerBoardPrefab;
    [SerializeField] private GameObject _hintPanelPrefab;
    [SerializeField] private GameObject _quitPanelPrefab;
    //     [SerializeField] private GameObject _hiddenTimerPrefab;
    
    [SerializeField] private HintPanelDisplay _hintPanelDisplay;
    [SerializeField] private GameObject _confetti;
    [SerializeField] private GameObject _solvedEffect;
    
    private List<PhraseObject> _phrasesDatabase;
    private QuitPanel _quitPanel;
    private Keyboard _keyboard;
    private CanvasGroup _keyboardCanvasGroup;
    private Timer _timer;
    private AnswerBoard _answerBoard;

    private IEnumerator currentCoroutine;

    private float _pregameTimer;

    // checking boolean to indicate that the game is running or not
    private bool _isAnswerCorrect;
    private bool _isVowelPicked;
    private bool _isGamePaused;

    public PlayPageState _playPageState
    {
        get;
        private set;
    }

    public enum PlayPageState
    {
        initialization,
        revealPuzzle,
        revealingPuzzle,
        puzzleRevealed,
        revealingPreselectedLetter,
        inputRevealVowel,
        vowelPicked,
        inputRevealChar,
        consonantPicked,
        revealingSelectedLetter,
        finishedPick,
        inputPhrases,
        ShowAnswer,
        endGame
    }

    public bool IsQuit { get; private set; }

    public bool IsGameEnd { get; private set; }

    // Use this for initialization
    private void Start()
    {
        //Time.timeScale = _cheatMode ? 4f : 1f;

        _isGamePaused = false;
        _playPageState = PlayPageState.initialization;

        // Instantiate all component in Play page
        _timer = AddPrefabToPlayPage(_timerPrefab, new Vector3(1f, 1f, 1f)).GetComponent<Timer>();
        _keyboard = AddPrefabToPlayPage(_keyboardPrefab, new Vector3(1f, 1f, 1f)).GetComponent<Keyboard>();
        _keyboardCanvasGroup = _keyboard.gameObject.GetComponent<CanvasGroup>();
        _answerBoard = AddPrefabToPlayPage(_answerBoardPrefab, new Vector3(1f, 1f, 1f)).GetComponent<AnswerBoard>();
        _quitPanel = AddPrefabToPlayPage(_quitPanelPrefab, new Vector3(1f, 1f, 1f)).GetComponent<QuitPanel>();

        _hintPanelDisplay = _answerBoard.GetComponentInChildren<HintPanelDisplay>();


        _quitPanel.OnYesButtonAction += Quit;
        _quitPanel.OnNoButtonAction += HideQuitPopup;
        _quitPanel.OnQuitButtonAction += ShowQuitPopup;
        _quitPanel.HidePopup();

        // Assign phrase from "Resources/PhraseDB" to _phrasesDatabase with Json format
        _phrasesDatabase = new List<PhraseObject>();
        var txt = (TextAsset)Resources.Load("PhraseDB", typeof(TextAsset));
        var json = txt.text;
        var data = JsonMapper.ToObject(json);
        for (var i = 0; i < data["Phrase"].Count; i++)
        {
            _phrasesDatabase.Add(new PhraseObject(data["Phrase"][i]["_PhraseText"].ToString(), data["Phrase"][i]["_Hint"].ToString()));
        }



        _answerBoard.PuzzleIsRevealed = false;

        //  InitGame();
    }

    /// <summary>
    /// Use to instantiate prefab with scale and set it to child of Play page
    /// </summary>
    private GameObject AddPrefabToPlayPage(GameObject prefab, Vector3 scale)
    {
        var instance = Instantiate(prefab);

        if (!instance)
        {
            Debug.LogError("[AddPrefabToPlayPage] : No prefab found");
        }
        instance.transform.SetParent(_playPage.transform, false);
        instance.transform.localScale = scale;
        instance.SetActive(true);

        return instance;
    }

    /// <summary>
    /// Use to set page as quit
    /// </summary>
    private void Quit()
    {
        IsQuit = true;
    }

    /// <summary>
    /// Use to hide quit popup
    /// </summary>
    private void HideQuitPopup()
    {
        _isGamePaused = false;

        _quitPanel.QuitButton.interactable = true;

        UnpauseGame();
        _quitPanel.HidePopup();
    }

    /// <summary>
    /// Use to show quit popup
    /// </summary>
    private void ShowQuitPopup()
    {
        _isGamePaused = true;

        _quitPanel.QuitButton.interactable = false;

        if (_answerBoard.PuzzleIsRevealed)
        {
            PauseGame();
            _quitPanel.ShowPopup();
        }
    }

    /// <summary>
    /// Use to pause game
    /// </summary>
    private void PauseGame()
    {
        _keyboardCanvasGroup.interactable = false;
        _answerBoard.Undisplay();

        _timer.PauseCountDown();
    }

    /// <summary>
    /// Use to unpause game
    /// </summary>
    private void UnpauseGame()
    {
        _keyboardCanvasGroup.interactable = true;
        _answerBoard.Display();

        _timer.ResumeCountDown();
    }

    /// <summary>
    /// Use to init the attributes and start the game
    /// </summary>
    private void InitGame()
    {
        // initialize phrase
        var rand = _cheatMode ? 0 : Random.Range(0, _phrasesDatabase.Count);
        //var rand = 0;
        _answerBoard.SetPhrase(_phrasesDatabase[rand]);

        // initialize keyboard
        InitKeyboard();
        _countdownSoundController.Stop();

        // initialize boolean
        _isAnswerCorrect = false;
        IsGameEnd = false;
        IsQuit = false;

        int lineCount = _answerBoard.DisplayLineCount;

        if (lineCount <= 2)
        {
            AudioSource.PlayClipAtPoint(_gameStartSound, GameObject.Find("Main Camera").gameObject.transform.position);
        }
        else
        {
            AudioSource.PlayClipAtPoint(_gameStartSound2, GameObject.Find("Main Camera").gameObject.transform.position);
        }
    }

    /// <summary>
    /// Use to initialize keyboard
    /// </summary>
    private void InitKeyboard()
    {
        _keyboard.Keys[0].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[0]);
            });
        _keyboard.Keys[1].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[1]);
            });
        _keyboard.Keys[2].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[2]);
            });
        _keyboard.Keys[3].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[3]);
            });
        _keyboard.Keys[4].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[4]);
            });
        _keyboard.Keys[5].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[5]);
            });
        _keyboard.Keys[6].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[6]);
            });
        _keyboard.Keys[7].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[7]);
            });
        _keyboard.Keys[8].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[8]);
            });
        _keyboard.Keys[9].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[9]);
            });
        _keyboard.Keys[10].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[10]);
            });
        _keyboard.Keys[11].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[11]);
            });
        _keyboard.Keys[12].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[12]);
            });
        _keyboard.Keys[13].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[13]);
            });
        _keyboard.Keys[14].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[14]);
            });
        _keyboard.Keys[15].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[15]);
            });
        _keyboard.Keys[16].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[16]);
            });
        _keyboard.Keys[17].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[17]);
            });
        _keyboard.Keys[18].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[18]);
            });
        _keyboard.Keys[19].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[19]);
            });
        _keyboard.Keys[20].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[20]);
            });
        _keyboard.Keys[21].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[21]);
            });
        _keyboard.Keys[22].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[22]);
            });
        _keyboard.Keys[23].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[23]);
            });
        _keyboard.Keys[24].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[24]);
            });
        _keyboard.Keys[25].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[25]);
            });
        _keyboard.Keys[26].Button.onClick.AddListener(() =>
            {
                OnKeyButton(_keyboard.Keys[26]);
            });

        // Submit Button
        _keyboard.Keys[Keyboard._buttonSubmitIndex].Button.onClick.AddListener(Submit);
        _keyboard.Keys[Keyboard._buttonGuessIndex].Button.onClick.AddListener(FinishRevealChar);

        // _keyboard.enableAllKeyboardButton();
        _keyboard.HideSubmit();

        _keyboard.SetUpCallBack();
    }

    /// <summary>
    /// Use to update on key button event
    /// </summary>
    private void OnKeyButton(KeyButton key)
    {
        _hintPanelDisplay.SetBoxDisable();
        _answerBoard.InputNewKey(key);
    }

    /// <summary>
    /// Use to skip input reveal character
    /// </summary>
    private void FinishRevealChar()
    {
        
//        if (_answerBoard.PickedLetterCount < 3)
//        {
//            _hintPanelDisplay.SetBoxEnable();
//            return;
//        }
        //_answerBoard.IsFinishedInputRevealChar = true;
        _answerBoard.FinishInputReveal();
        _keyboard.enableAllKeyboardButton();

        _keyboard.disableGuess();
        _keyboard.HideGuess();
        _keyboard.ShowSubmit();
        
        _keyboard.disableRSTLNEkey();
        _keyboard.DisablePickedLetters();

    }

    [HideInInspector] public bool EndGameCalled = false;

    /// <summary>
    /// Use to submit answer, if correct then end game else reset input 
    /// </summary>
    private void Submit()
    {
        _isAnswerCorrect = _answerBoard.CheckAnswer();

        if (_isAnswerCorrect)
        {
            EndGame();
        }
        else
        {
            AudioSource.PlayClipAtPoint(_wrongSound, GameObject.Find("Main Camera").gameObject.transform.position);
            _answerBoard.ResetInputPhrase();
        }
    }

    /// <summary>
    /// Use to end the game
    /// </summary>
    private void EndGame()
    {
        if (EndGameCalled)
            return;
        
        EndGameCalled = true;
        _isAnswerCorrect = _answerBoard.CheckAnswer();

        _keyboard.disableAllKeyboardButton();
        _keyboard.disableKeyboard();

        _timer.IsTimerRunning = false;

        _quitPanel.QuitButton.interactable = false;

        if (_isAnswerCorrect)
        {
            _keyboard.transform.DOLocalMove(new Vector3(_keyboard.transform.localPosition.x, -2000f), 1f);//.SetEase(Ease.InBack);


            AudioSource.PlayClipAtPoint(_correctSound, GameObject.Find("Main Camera").gameObject.transform.position);
            AudioSource.PlayClipAtPoint(_crowdSound, GameObject.Find("Main Camera").gameObject.transform.position);

            _hintPanelDisplay.GetComponent < CanvasGroup >().DOFade(0f, 0.3f);
            _answerBoard.transform.DOLocalMoveY(-200f, 2f);

            GameObject confetti = null;

            if (_confetti)
            {
                confetti = Instantiate < GameObject >(_confetti);
                confetti.transform.position = new Vector3(0f, 150f, 0f);
            }

            GameObject solvedEffect = null;

            if (_solvedEffect)
            {
                solvedEffect = Instantiate<GameObject>(_solvedEffect);
                solvedEffect.transform.SetParent(transform, false);
            }

            //_hintPanelDisplay.SetCongratulationPanel();

            StartCoroutine(WaitForSecondsAndDoFunction(5, () =>
                    {
                        _pageAnimation.Play(_animatorOutStateName);
                        _playPageState = PlayPageState.endGame;
                        
                        if (confetti)
                            confetti.SetActive(false);

                      
                    }));

        }
        else
        {
            _answerBoard.ShowAnswer();

            Debug.Log("is it showing this?");
            _answerBoard.PlayHintAnimationInNonInvasive(null, "Better luck next time");

            AudioSource.PlayClipAtPoint(_timeUpSound, GameObject.Find("Main Camera").gameObject.transform.position);
            _playPageState = PlayPageState.ShowAnswer;
            StartCoroutine(WaitForSecondsAndDoFunction(5, () =>
                    {
                       
                        _pageAnimation.Play(_animatorOutStateName);
                        _playPageState = PlayPageState.endGame;
                    }));
        }
    }

    private IEnumerator WaitForSecondsAndDoFunction(float waitTime, Action actionFunction)
    {
        yield return new WaitForSeconds(waitTime);
        actionFunction();
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    private void Update()
    {

        if (_playPageState == PlayPageState.initialization)
        {
            _keyboard.disableAllKeyboardButton();
            _keyboard.disableKeyboard();

            _pregameTimer = 1f;

            _timer.ResetTimer(20);

            _timer.IsTimerStart = false;

            _playPageState = PlayPageState.revealPuzzle;
        }
        if (_playPageState == PlayPageState.revealPuzzle)
        {
            _pregameTimer -= Time.deltaTime;
            
            _timer.IsTimerRunning = false;

            if (_pregameTimer <= 0)
            {
                _playPageState = PlayPageState.revealingPuzzle;
            }
        }
        if (_playPageState == PlayPageState.revealingPuzzle)
        {
            InitGame();

            _playPageState = PlayPageState.puzzleRevealed;
        }
        if (_playPageState == PlayPageState.puzzleRevealed)
        {
            if (_answerBoard.IsDisplayInitailized)
            {
                currentCoroutine = _answerBoard.RevealPhraseOverTime();
                StartCoroutine(currentCoroutine);

                _playPageState = PlayPageState.revealingPreselectedLetter;
            }
        }

        if (_playPageState == PlayPageState.revealingPreselectedLetter)
        {
            if (_answerBoard.PuzzleIsRevealed)
            {
                _timer.IsTimerRunning = true;
                _timer.ResumeCountDown();
                
                StartCoroutine(WaitForSecondsAndDoFunction(0.3f, () =>
                        {
                            _answerBoard.PlayHintAnimationIn
                        (
                                () =>
                                {
                                    _timer.StartCountDown();
                                    _keyboard.enableKeyboard();
                                    _keyboard.enableAllKeyboardButton();
                                    _keyboard.disableRSTLNEkey();
                                    _keyboard.disableVowels();
                                    _keyboard.disableBackspace();
                                    _keyboard.disableSubmit();
                                }
                            );
                        }));
                


                _playPageState = PlayPageState.inputRevealChar;
            }
        }

        if (_playPageState == PlayPageState.inputRevealChar)
        {
            // Called Once
            if (_isGamePaused && _quitPanel.QuitButton.IsActive())
            {
                ShowQuitPopup();
            }

            // Debug.Log(_answerBoard.PickedLetterCount +" && " + _keyboard.PickedLetterKeys.Count);
            if (_answerBoard.PickedLetterCount > _keyboard.PickedLetterKeys.Count)
            {
                // Debug.Log("Inputted");

                for (int i = 0; i < _keyboard.Keys.Count; i++)
                {
                    // Debug.Log(_keyboard.Keys[i].Key + " "+ _answerBoard.RevealedConsonants[_answerBoard.RevealedConsonants.Length - 1].ToString());
                    if (_keyboard.Keys[i].Key == _answerBoard.RevealedConsonants[_answerBoard.RevealedConsonants.Length - 1].ToString())
                    {
                        _keyboard.PickedLetterKeys.Add(_keyboard.Keys[i]);

                        break;
                    }
                }

                _hintPanelDisplay.SetRevealedCharAt(_keyboard.PickedLetterKeys.Count - 1, _keyboard.PickedLetterKeys[_keyboard.PickedLetterKeys.Count - 1].Key);

                if ((3 - _keyboard.PickedLetterKeys.Count) > 1)
                {

                    _hintPanelDisplay.SetIntroText("Choose " + (3 - _keyboard.PickedLetterKeys.Count) + " more consonants");
                }
                else
                {

                    _hintPanelDisplay.SetIntroText("Choose 1 more consonant");
                }
            }

            // go to next state
            if (_answerBoard.PickedLetterCount == 3)
            {
                _playPageState = PlayPageState.consonantPicked;
            }
            
            
        }
        if (_playPageState == PlayPageState.consonantPicked)
        {

            _keyboard.disableAllKeyboardButton();

            _playPageState = PlayPageState.inputRevealVowel;

            _timer.PauseCountDown();

            _answerBoard.PlayHintAnimationInNonInvasive 
            (
                () =>
                {
                    _timer.ResumeCountDown();
                    _keyboard.enableVowels();
                    _keyboard.enableGuess();
                },

                "Choose a vowel"
            );
            
        }

        if (_playPageState == PlayPageState.inputRevealVowel)
        {
            // Check if vowel were picked
            if (_answerBoard.PickedLetterCount >= 4)
            {
                // Detect the vowel picked and store it in keyboard
                for (int i = 0; i < _keyboard.Keys.Count; i++)
                {
                    if (_keyboard.Keys[i].Key == _answerBoard.RevealedVowel.ToString())
                    {
                        _keyboard.PickedLetterKeys.Add(_keyboard.Keys[i]);
                        break;
                    }
                }


                _hintPanelDisplay.SetRevealedCharAt(_keyboard.PickedLetterKeys.Count - 1, _keyboard.PickedLetterKeys[_keyboard.PickedLetterKeys.Count - 1].Key);

                // move to next phase
                _playPageState = PlayPageState.vowelPicked;
            }
        }
        if (_playPageState == PlayPageState.vowelPicked)
        {
            _playPageState = PlayPageState.revealingSelectedLetter;
        }

        if ((_playPageState == PlayPageState.inputRevealVowel || _playPageState == PlayPageState.inputRevealChar))
        {
            if (_timer.timeCounter <= 10 && !_countdownSoundController.isPlaying)
            {
                _countdownSoundController.Play();
            }
        }

        if ((_playPageState == PlayPageState.inputRevealVowel || _playPageState == PlayPageState.inputRevealChar) && (_answerBoard.IsFinishedInputRevealChar || _timer.IsTimeUp))
        {
            _answerBoard.IsFinishedInputRevealChar = true;
            _playPageState = PlayPageState.revealingSelectedLetter;

            if (_timer.IsTimeUp)
            {
                AudioSource.PlayClipAtPoint(_timeUpSound, GameObject.Find("Main Camera").gameObject.transform.position);
            }
        }

        if (_playPageState == PlayPageState.revealingSelectedLetter)
        {
            _countdownSoundController.Stop();
            _timer.IsTimerRunning = false;
            
            _answerBoard.PuzzleIsRevealed = false;
            currentCoroutine = _answerBoard.RevealSelectedLetterOverTime();
            StartCoroutine(currentCoroutine);
            _playPageState = PlayPageState.finishedPick;
        }

        if (_playPageState == PlayPageState.finishedPick)
        {
            if (_answerBoard.PuzzleIsRevealed)
            {
                foreach (KeyButton key in _keyboard.PickedLetterKeys)
                {
                    KeyButton temp = key;
                }
                
                _timer.IsTimerStart = true;
                _timer.IsTimerRunning = true;

                _timer.ResetTimer(30);

                FinishRevealChar();
                
                if (_answerBoard.IsFinishedInput)
                {
                    _isAnswerCorrect = _answerBoard.CheckAnswer();
                    // Should be called once
                    _playPageState = PlayPageState.ShowAnswer;
                    EndGame();

                    return;
                }

                _keyboard.disableAllKeyboardButton();

                //_timer.StopCountDown();
                _timer.PauseCountDown();
                _answerBoard.PlayHintAnimationOutThenIn 
                (
                    () =>
                    {
                        _timer.ResumeCountDown();
                        _keyboard.enableAllKeyboardButton();

                        _keyboard.disableGuess();
                        _keyboard.HideGuess();
                        _keyboard.ShowSubmit();

                        _keyboard.disableRSTLNEkey();
                        _keyboard.DisablePickedLetters();
                    },

                    "Solve the puzzle."
                );
                
                _answerBoard.SetCursor();

                _playPageState = PlayPageState.inputPhrases;
            }
        }

        if (_playPageState == PlayPageState.inputPhrases)
        {
            if (_timer.IsTimeUp)
            {
                EndGame();
            }

            if (_answerBoard.IsFinishedInput)
            {
                _keyboard.enableSubmit();
            }
            else
            {
                _keyboard.disableSubmit();
            }
        }
        else if (_playPageState == PlayPageState.endGame)
        {

            IsGameEnd = true;

            if (!_pageAnimation.isPlaying)
            {
                Destroy(gameObject);
            }
        }
    }
}
