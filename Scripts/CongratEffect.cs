﻿using UnityEngine;

public class CongratEffect : MonoBehaviour
{
    [SerializeField] private GameObject _fireworkPrefab;

    // Use this for initialization
    void Start ()
    {
        var tempObject = Instantiate(_fireworkPrefab);
        tempObject.transform.SetParent(gameObject.transform, false);
        tempObject.transform.localEulerAngles = new Vector3(0, 180, 180);
    }
}
