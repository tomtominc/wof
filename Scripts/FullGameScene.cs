﻿using System;
using UnityEngine;
using System.Collections;
using LitJson;
using System.IO;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;

public class FullGameScene : MonoBehaviour
{
    public static FullGameScene Instance;

    [SerializeField] private AudioClip _pageTransitionSound;

    [SerializeField] private AudioClip _introSoundtrack;
    [SerializeField] private AudioClip _gameplaySoundtrack;

    [SerializeField] private AudioSource _soundtrackController;


    [SerializeField] private Animation _sony;
    [SerializeField] private Background _background;
    [SerializeField] private Image _in2WinForeground;
    [SerializeField] private float _fadeToWhiteDuration = 1.0f;
    [SerializeField] private GameObject _titlePagePrefab;
    [SerializeField] private GameObject _playPagePrefab;
    [SerializeField] private GameObject _wheelPagePrefab;
    [SerializeField] private GameObject _transitionLightPrefab;
    [SerializeField] private SpriteRenderer _blackScreen;
    private TitlePage _titlePage;
    private PlayPage _playPage;
    private WheelPage _wheelPage;
    private GameState _gameState;

    public List<RewardItem> RewardItems = new List<RewardItem>();

    public int RewardPieIndex;
    public Sprite CouponSprite;

    private enum GameState
    {
        sony,
        animateBackground,
        titlePage,
        playPage,
        wheelPage
    }

    private IEnumerator SetRewardPieIndex()
    {
        string streamingPath = string.Format("file:///{0}/wheelPrize.json", Application.streamingAssetsPath);

        WWW stream = new WWW(streamingPath);

        while (!stream.isDone)
            yield return null;

        if (string.IsNullOrEmpty(stream.error))
        {
            JsonData json = JsonMapper.ToObject(stream.text);
            RewardPieIndex = (int)json["winningIndex"];
        }
        else
        {
            Debug.LogWarningFormat("Error No wheelPrize.json found: {0}", stream.error);
            RewardPieIndex = RandomRewardpieIndex();
        }
    }

    private IEnumerator SetCouponImage()
    {
        string streamingPath = string.Format("file:///{0}/couponImage.png", Application.streamingAssetsPath);
        WWW stream = new WWW(streamingPath);

        Debug.Log(streamingPath);

        while (!stream.isDone)
            yield return null;

        if (string.IsNullOrEmpty(stream.error) && stream.texture != null)
        {
            Rect rect = new Rect(0f, 0f, stream.texture.width, stream.texture.height);

            CouponSprite = Sprite.Create(stream.texture, rect, new Vector2(0.5f, 0.5f));
        }
        else
        {
            Debug.LogWarningFormat("Error no coupon sprite found: {0}", stream.error);
        }

    }


    public int RandomRewardpieIndex()
    {
        Selector selector = new Selector();
        RewardItem rewardItem = selector.Single(RewardItems);
        return RewardItems.IndexOf(rewardItem);
    }

    private void InitWheelRewards()
    {
        // Read RewardText from file "Assets/Resources/ItemRewards.txt" with json format and add them to _rewardItems
        TextAsset txt = (TextAsset)Resources.Load("ItemRewards", typeof(TextAsset));

        string json = txt.text;

        JsonData jsonData = JsonMapper.ToObject(json);

        for (var i = 0; i < jsonData["TextInPie"].Count; i++)
        {
            RewardItems.Add(new RewardItem(jsonData["TextInPie"][i].ToString(), 
                    float.Parse(jsonData["Angle"][i].ToString()), 
                    jsonData["PieColor"][i].ToString(), 
                    float.Parse(jsonData["Probability"][i].ToString())));
        }
        StartCoroutine(SetCouponImage());

        StartCoroutine(SetRewardPieIndex());
       
    }

    // Use this for initialization
    private void Start()
    {
        Instance = this;

        //Screen.SetResolution(Screen.height / 16 * 9, Screen.height, false);
        _gameState = GameState.sony;

        _soundtrackController.clip = _introSoundtrack;
        _soundtrackController.Play();

        InitWheelRewards();
    }

    // Update is called once per frame
    private void Update()
    {
        if (_gameState == GameState.sony && !_sony.isPlaying)
        {
            _background.gameObject.SetActive(true);
            _gameState = GameState.animateBackground;
        }
        else if (_gameState == GameState.animateBackground && _background.IsAllAnimationFinished)
        {
            _titlePage = Instantiate(_titlePagePrefab).GetComponent<TitlePage>();
            _gameState = GameState.titlePage;
        }
        else if (_gameState == GameState.titlePage && _titlePage != null && _titlePage.IsTitlePageEnd)
        {
            Instantiate(_transitionLightPrefab);
            StartCoroutine(WaitForSecondsAndDoFunction(0.8f, () =>
                    {
                        _playPage = Instantiate(_playPagePrefab).GetComponent<PlayPage>();
                    }));
            _gameState = GameState.playPage;


            AudioSource.PlayClipAtPoint(_pageTransitionSound, GameObject.Find("Main Camera").gameObject.transform.position);
            // change song here
            _soundtrackController.clip = _gameplaySoundtrack;
            // _soundtrackController.Play();


        }
        else if (_gameState == GameState.playPage && _playPage != null)
        {
            if (_playPage._playPageState == PlayPage.PlayPageState.revealingPreselectedLetter)
            {
                _soundtrackController.Play();
            }

            if (_playPage.EndGameCalled)
            {
                if (_soundtrackController.isPlaying)
                    _soundtrackController.Stop();
            }

            if (_playPage.IsGameEnd)
            {
                Instantiate(_transitionLightPrefab);
                StartCoroutine(WaitForSecondsAndDoFunction(0.8f,
                        () =>
                        {
                            _wheelPage = Instantiate(_wheelPagePrefab).GetComponent<WheelPage>();
                        }));

                AudioSource.PlayClipAtPoint(_pageTransitionSound, GameObject.Find("Main Camera").gameObject.transform.position);
                _soundtrackController.Stop();
                _gameState = GameState.wheelPage;
            }
            else if (_playPage.IsQuit)
            {
                FadeToWhite();
            }
        }
        else if (_gameState == GameState.wheelPage && _wheelPage != null)
        {
            if (_wheelPage.IsWheelPageEnd)
            {
                FadeToWhite();
            }
            else if (_wheelPage.IsRewardPieShown)
            {
            }
        }
    }

    private void FadeToWhite()
    {
        _in2WinForeground.DOFade(1f, _fadeToWhiteDuration).OnComplete(() =>
            {
                Application.Quit();
            });

    }

    private IEnumerator WaitForSecondsAndDoFunction(float waitTime, Action actionFunction)
    {
        yield return new WaitForSeconds(waitTime);
        actionFunction();
    }

    // Use this for update app's resources file base on server's resources file ( update PhraseDB and then update ItemRewards )
    //IEnumerator UpdateFileResources()
    //{
    //    TextAsset textAsset;       // TextAsset that get get load resource file
    //    string json;               // String json that is in resource file
    //    JsonData dataInApp;        // Oject that mapping with json form app's resource file
    //    JsonData dataFromWeb;      // Oject that mapping with json form server's resource file
    //    WWW w;                      // WWW parameter that use to connect website

    //    // Get oject form Resources/PhraseDB.txt
    //    textAsset = (TextAsset)Resources.Load("PhraseDB", typeof(TextAsset));
    //    json = textAsset.text;
    //    dataInApp = JsonMapper.ToObject(json);

    //    // Connect to website to get updated resource file
    //    w = new WWW("http://testwebsite.com/PhraseDB");
    //    yield return w;
    //    if (w.error != null)
    //    {
    //        // Show WWW error (Example : "Debug.Log("Error .. " + w.error);")
    //        // Debug.Log("Error .. " + w.error);
    //    }
    //    else
    //    {
    //        // Show data that get form url
    //        // Debug.Log("Found ... ==>" + w.text + "<==");

    //        // Get oject form url
    //        json = w.text;
    //        dataFromWeb = JsonMapper.ToObject(json);

    //        // Save new json file if data in app is not updated ( Condition is App's data date and Server's data date )
    //        if ( dataFromWeb["Date"] != dataInApp["Date"])
    //        {
    //            string path;
    //            #if UNITY_EDITOR
    //                path = "Assets/Resources/PhraseDB.txt";
    //            #endif
    //            #if UNITY_STANDALONE
    //                // You cannot add a subfolder, at least it does not work for me
    //                path = "MyGame_Data/Resources/ItemInfo.json";
    //            #endif
    //                using (FileStream fs = new FileStream(path, FileMode.Create))
    //                {
    //                    using (StreamWriter writer = new StreamWriter(fs))
    //                    {
    //                        writer.Write(json);
    //                    }
    //                }
    //            #if UNITY_EDITOR
    //                UnityEditor.AssetDatabase.Refresh();
    //            #endif
    //        }
    //    }


    //    // Get oject form Resources/ItemRewards.txt
    //    textAsset = (TextAsset)Resources.Load("ItemRewards", typeof(TextAsset));
    //    json = textAsset.text;
    //    dataInApp = JsonMapper.ToObject(json);

    //    // Connect to website to get updated resource file
    //    w = new WWW("http://testwebsite.com/ItemRewards");
    //    yield return w;
    //    if (w.error != null)
    //    {
    //        // Show WWW error (Example : "Debug.Log("Error .. " + w.error);")
    //        // Debug.Log("Error .. " + w.error);
    //    }
    //    else
    //    {
    //        // Show data that get form url
    //        // Debug.Log("Found ... ==>" + w.text + "<==");

    //        // Get oject form url
    //        json = w.text;
    //        dataFromWeb = JsonMapper.ToObject(json);


    //        // Save new json file if data in app is not updated ( Condition is App's data date and Server's data date )
    //        if (dataFromWeb["Date"] != dataInApp["Date"])
    //        {
    //            string path;
    //            #if UNITY_EDITOR
    //                path = "Assets/Resources/ItemRewards.txt";
    //            #endif
    //            #if UNITY_STANDALONE
    //                // You cannot add a subfolder, at least it does not work for me
    //                path = "MyGame_Data/Resources/ItemInfo.json";
    //            #endif
    //                using (FileStream fs = new FileStream(path, FileMode.Create))
    //                {
    //                    using (StreamWriter writer = new StreamWriter(fs))
    //                    {
    //                        writer.Write(json);
    //                    }
    //                }
    //            #if UNITY_EDITOR
    //                UnityEditor.AssetDatabase.Refresh();
    //            #endif
    //        }
    //    }
    //}
}
