﻿using UnityEngine;
using System.Collections.Generic;

public class NumberInPie : MonoBehaviour
{
    [SerializeField] private List<SpriteRenderer> _numbers = new List<SpriteRenderer>();
    [SerializeField] private List<SpriteRenderer> _shadows = new List<SpriteRenderer>();
    [SerializeField] private Sprite[] _numberSprites = new Sprite[10];
    [SerializeField] private Sprite[] _shadowSprites = new Sprite[10];

    public void SetNumber(string inputNumber)
    {
        for (var i = 0; i < _numbers.Count; i++)
        {
            var number = int.Parse(inputNumber[i].ToString());
            _numbers[i].sprite = _numberSprites[number];
            _shadows[i].sprite = _shadowSprites[number];
        }
    }

    public void SetSortingOrder(int orderNumber)
    {
        foreach (var number in _numbers)
        {
            number.sortingOrder = orderNumber;
        }

        foreach (var shadow in _shadows)
        {
            shadow.sortingOrder = orderNumber -1;
        }
    }
}
