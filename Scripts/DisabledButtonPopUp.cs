﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisabledButtonPopUp : MonoBehaviour {

    private float createAnimTime = 0f;
    private float appearTime = 1f;
    private float deleteAnimTime = 0f;

    private float timer;

   
    private PopUpState thisPopUpState;

    private enum PopUpState
    {
        creating,
        appear,
        deleting
    }

	// Use this for initialization
	void Start () {
        thisPopUpState = PopUpState.creating;
        timer = createAnimTime;
    }

    // Update is called once per frame
    void Update() {

        if( timer > 0)
        {
            timer -= Time.deltaTime;
        }

        if (thisPopUpState == PopUpState.creating)
        {
            if (timer <= 0)
            {
                timer = appearTime;
                thisPopUpState = PopUpState.appear;
            }
        }
        else if(thisPopUpState == PopUpState.appear)
        {

            if (timer <= 0)
            {
                timer = deleteAnimTime;
                thisPopUpState = PopUpState.deleting;
            }
        }
        else if(thisPopUpState == PopUpState.deleting)
        {
            if (timer <= 0)
            {
                GameObject.Destroy(this.gameObject);
            }
        }
        else
        {
            // ERROR naa
        }
	}
}
