﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class DisplayLineScript : MonoBehaviour
{
    public List<DisplayBlockScript> _blocks = new List<DisplayBlockScript>();
    public int StartIndex { get; set; }

    private string _answerPhraseInLine;
    private string _phraseInLine;
    private BlockStyle[] _blockStyles;

    public bool IsAssigned
    {
        get
        {
            if (_answerPhraseInLine != null)
            {
                return true;
            }
            return false;
        }
    }

    private enum BlockStyle
    {
        Close, Open, Blue, Red, Orange
    }

    /// <summary>
    /// Use to assign answer word in this line
    /// </summary>
    public void AssignAnswerPhraseInLine(string phraseInLine)
    {
        if (StartIndex + phraseInLine.Length > _blocks.Count) return;

        _answerPhraseInLine = phraseInLine;
        _phraseInLine = new string(' ', phraseInLine.Length);
        _blockStyles = new BlockStyle[phraseInLine.Length];
        _blockStyles.Fill(BlockStyle.Open);
        var index = StartIndex;
        foreach (var letter in phraseInLine)
        {
            if(letter == ' ')
                _blockStyles[index- StartIndex] = BlockStyle.Close;
            _blocks[index].AssignAnswerLetter(letter);
            index++;
        }
    }

    /// <summary>
    /// Use to assign a word that will show in this line
    /// </summary>
    public void AssignShowPhraseInLine(string phraseInLine)
    {
        if (StartIndex + phraseInLine.Length > _blocks.Count) return;

        for (var i = 0; i < phraseInLine.Length; i++)
        {
            if (_phraseInLine[i] != phraseInLine[i])
            {
                _blocks[StartIndex+i].AssignShowLetter(phraseInLine[i]);
                _blockStyles[i] = BlockStyle.Blue;
            }
            else
            {
                if (_answerPhraseInLine[i] != ' ')
                {
                    _blockStyles[i] = BlockStyle.Open;
                }
            }
        }
        _phraseInLine = phraseInLine;
    }

    /// <summary>
    /// Use to 
    /// </summary>
    public void SetLightEmptyInLine(bool isWrongAnswer = false)
    {
        var tempBlockStyle = BlockStyle.Blue;
        if(isWrongAnswer)
            tempBlockStyle = BlockStyle.Red;

        if (_phraseInLine == null) return;
        for (var i = 0; i < _phraseInLine.Length; i++)
        {
            if (_phraseInLine[i] == ' ' && _answerPhraseInLine[i] != ' ')
            {
                _blockStyles[i] = tempBlockStyle;
            }
            else if(_answerPhraseInLine[i] != ' ')
            {
                _blockStyles[i] = BlockStyle.Open;
            }
        }
    }

    public void SetCursorInLine(int index)
    {
        Debug.Log("_blockStyles["+index+"] = "+ _blockStyles[index]);
        if(_blockStyles[index] == BlockStyle.Blue ||_blockStyles[index] == BlockStyle.Red)
            _blockStyles[index] = BlockStyle.Orange;
    }

    /// <summary>
    /// Use to get number of blocks in this line
    /// </summary>
    public int GetNumOfBlocks()
    {
        return _blocks.Count;
    }

    /// <summary>
    /// Use to display all opened blocks in this line
    /// </summary>
    public IEnumerator RevealPuzzleLine()
    {
        if (_blockStyles == null) yield break;
        for (var i = 0; i < _blockStyles.Length; i++)
        {
            yield return new WaitForSeconds(0.02f);
            if (_blockStyles[i] != BlockStyle.Close)
            {
                _blocks[StartIndex + i].OpenThisBlock();
                _blocks[StartIndex + i].Unlight();
            }

            if (_blockStyles[i] == BlockStyle.Blue)
            {
                _blocks[StartIndex + i].SetLightBlue();
                _blocks[StartIndex + i].Unlight();
            }
            else if (_blockStyles[i] == BlockStyle.Red)
            {
                _blocks[StartIndex + i].SetLightRed();
                _blocks[StartIndex + i].Unlight();
            }
            else if (_blockStyles[i] == BlockStyle.Orange)
            {
                // _blocks[StartIndex + i].SetLightOrange();
                _blocks[StartIndex + i].SetCursorLight();
                // _blocks[StartIndex + i].Light();
            }

            yield return null;
        }
    }

    /// <summary>
    /// Use to display all opened blocks in this line
    /// </summary>
    public void DisplayThisLine()
    {
        if (_blockStyles == null) return;
        for (var i = 0; i < _blockStyles.Length; i++)
        {
            if (_blockStyles[i] != BlockStyle.Close)
            {
                _blocks[StartIndex + i].OpenThisBlock();
                _blocks[StartIndex + i].Unlight();
            }

            if (_blockStyles[i] == BlockStyle.Blue)
            {
                _blocks[StartIndex + i].SetLightBlue();
                _blocks[StartIndex + i].Unlight();
            }
            else if (_blockStyles[i] == BlockStyle.Red)
            {
                _blocks[StartIndex + i].SetLightRed();
                _blocks[StartIndex + i].Unlight();
            }
            else if (_blockStyles[i] == BlockStyle.Orange)
            {
                // _blocks[StartIndex + i].SetLightOrange();
                _blocks[StartIndex + i].SetCursorLight();
                // _blocks[StartIndex + i].Light();
            }
            
        }
    }

    /// <summary>
    /// Use to undisplay all blocks in this line
    /// </summary>
    public void UndisplayThisLine()
    {
        if (_blockStyles == null) return;
        for (var i = 0; i < _blockStyles.Length; i++)
        {
            if (_blockStyles[i] == BlockStyle.Close) continue;
            _blocks[StartIndex + i].CloseThisBlock();
            _blocks[StartIndex + i].Unlight();
        }
    }

    /// <summary>
    /// Use to light a cursor at a specific index
    /// </summary>
    public void LightCursorAtIndex(int index)
    {
        UnlightCursorAllBlocksInLine();
        _blocks[index].SetLightRed();
        _blocks[index].Light();
    }

    /// <summary>
    /// Use to unlight all block in line
    /// </summary>
    public void UnlightCursorAllBlocksInLine()
    {
        foreach (var block in _blocks)
        {
            block.Unlight();
        }
    }

    /// <summary>
    /// Use to unlight all block in line
    /// </summary>
    public void ShowAnswerInLine()
    {
        foreach (var block in _blocks)
        {
            block.ShowAnswer();
            block.Unlight();
        }
    }

    // Use this for initialization
    private void Awake()
    {
        StartIndex = 0;
    }
}
