﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class QuitPanel : MonoBehaviour
{
    private static float MAX_INACTIVITY_TIME = 15;

    public event Action OnYesButtonAction = delegate { };
    public event Action OnNoButtonAction = delegate { };
    public event Action OnQuitButtonAction = delegate { };

    [SerializeField] private GameObject _popupPanel;
    [SerializeField] private GameObject _quitButton;
        
    public Button QuitButton
    {
        get
        {
            return _quitButton.GetComponent<Button>();
        }
    }

    public float inactivityTimer = 0;

    private bool PopUpIsOn
    {
        get {
            return _popupPanel.activeSelf;
        }
    }

    void Update()
    {
        if (PopUpIsOn) {
            inactivityTimer -= Time.deltaTime;
            
            if (inactivityTimer <= 0)
            {
                OnNoButton();
            }
        }
    }

    // Use to hide this quit popup
    public void HidePopup()
    {
        _quitButton.SetActive(true);
        _popupPanel.SetActive(false);
    }

    // Use to show this quit popup
    public void ShowPopup()
    {
        _quitButton.SetActive(false);
        _popupPanel.SetActive(true);

        inactivityTimer = MAX_INACTIVITY_TIME;
    }

    // Use to on yes button event
    public void OnYesButton()
    {
        OnYesButtonAction();
    }

    // Use to on no button event
    public void OnNoButton()
    {
        OnNoButtonAction();
    }

    // Use to on quit button event
    public void OnQuitButton()
    {
        OnQuitButtonAction();
    }
}
