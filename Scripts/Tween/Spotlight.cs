﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Spotlight : MonoBehaviour
{
    public Transform rotationPoint;
    public Vector3 rotationEndValue;
    public float rotationDuration;
    public RotateMode rotateMode;

    public void Start()
    {
        rotationPoint.DOLocalRotate(rotationEndValue, rotationDuration, rotateMode).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);    
    }

 
}
