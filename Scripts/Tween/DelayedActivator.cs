﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DelayedActivator : MonoBehaviour
{
    public float delay = 2f;

    public List < GameObject > gosToActive;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);

        gosToActive.ForEach(x => x.SetActive(true));
    }
}
