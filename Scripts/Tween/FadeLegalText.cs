﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class FadeLegalText : MonoBehaviour
{
    private Text text;
    public float fadeDelay;
    public float fadeDuration;

    public void Start()
    {
        text = GetComponent < Text >();

        text.DOFade(0f, fadeDuration).SetDelay(fadeDelay);
    }
}
