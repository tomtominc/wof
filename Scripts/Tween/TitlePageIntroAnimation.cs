﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TitlePageIntroAnimation : MonoBehaviour
{
    [Header("Components")]
    public RectTransform wheel;
    public RectTransform logoParent;

    public RectTransform logoPartWheel;
    public RectTransform logoPartOf;
    public RectTransform logoPartFortune;

    [Header("Wheel Tween Options")]
    public Vector3 wheelScaleP1;
    public float wheelScaleDurationP1;

    public Vector3 wheelScaleP2;
    public float wheelScaleDurationP2;
    public float wheelScaleDelayP2;

    public float wheelScaleDurationP3;
    public float wheelScaleDelayP3;

    public Vector3 wheelPosP1;
    public float wheelMoveDurationP1;

    public Vector3 wheelPosP2;
    public float wheelMoveDurationP2;

    [Header("Logo Parent Tween Options")]
    public Vector3 logoParentPos;

    [Header("Logo Part Tween Options")]
    public Vector3 logoPartWheelOffset;
    public float logoPartWheelMoveDuration;
    public float logoPartWheelMoveDelay;

    public Vector3 logoPartOfOffset;
    public float logoPartOfMoveDuration;
    public float logoPartOfMoveDelay;

    public Vector3 logoPartFortuneOffset;
    public float logoPartFortuneMoveDuration;
    public float logoPartFortuneMoveDelay;


    [Header("Delay Options")]
    public float p1StartDelay;
    public float p2StartDelay;

    public void Start()
    {
        Sequence sequence = DOTween.Sequence();

        sequence.AppendInterval(p1StartDelay);
        sequence.Append(wheel.DOScale(wheelScaleP1, wheelScaleDurationP1));

        sequence.AppendInterval(wheelScaleDelayP2);
        sequence.Append(wheel.DOScale(wheelScaleP2, wheelScaleDurationP2));
        sequence.Join(wheel.DOLocalMove(wheelPosP1, wheelMoveDurationP1));

        sequence.AppendInterval(logoPartWheelMoveDelay);
        sequence.Append(logoPartWheel.DOLocalMove(logoPartWheelOffset, logoPartWheelMoveDuration).SetEase(Ease.OutBack));

        sequence.AppendInterval(logoPartOfMoveDelay);
        sequence.Append(logoPartOf.DOLocalMove(logoPartOfOffset, logoPartOfMoveDuration).SetEase(Ease.OutBack));

        sequence.AppendInterval(logoPartFortuneMoveDelay);
        sequence.Append(logoPartFortune.DOLocalMove(logoPartFortuneOffset, logoPartFortuneMoveDuration).SetEase(Ease.OutBack));

        sequence.AppendInterval(wheelScaleDelayP3);
        sequence.Append(wheel.DOScale(Vector3.one, wheelScaleDurationP3));

        sequence.Join(wheel.DOLocalMove(wheelPosP2, wheelMoveDurationP2));
        sequence.Join(logoParent.DOLocalMove(logoParentPos, wheelMoveDurationP2));
        sequence.Join(logoParent.DOScale(Vector3.one, wheelMoveDurationP2));


    }



}
