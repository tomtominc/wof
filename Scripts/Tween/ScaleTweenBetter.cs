﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ScaleTweenBetter : MonoBehaviour
{
    [SerializeField] private Vector3 _scaleTo;
    [SerializeField] private float _scaleToDuration;
    [SerializeField] private float _scaleToDelay;

    [SerializeField] private int _loop = -1;
    [SerializeField] private LoopType _loopType = LoopType.Yoyo;

    public void Start()
    {
        DoEffect();
    }

    public void DoEffect()
    {
        transform.DOScale(_scaleTo, _scaleToDuration).SetDelay(_scaleToDelay).SetLoops(_loop, _loopType);
    }
}
