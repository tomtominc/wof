﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class FadeInTween : MonoBehaviour
{
    private Image _image;

    [SerializeField]private float _fadeInDuration;
    [SerializeField]private float _fadeInDelay;
    [SerializeField]private Text _optionalText;

    void Start()
    {
        _image = GetComponent < Image >();

        _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 0f);

        _image.DOFade(1f, _fadeInDuration).SetDelay(_fadeInDelay);

        if (_optionalText)
        {
            _optionalText.DOFade(1f, _fadeInDuration).SetDelay(_fadeInDelay);
        }
    }
}
