﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class ScaleTween : MonoBehaviour
{
    [SerializeField] private Vector3 _scaleTo;
    [SerializeField] private float _scaleToDuration;
    [SerializeField] private float _scaleToDelay;
    [SerializeField] private float _scaleBackDuration;
    [SerializeField] private float _scaleBackDelay;
    [SerializeField] private Vector2 _shadowEffectDistance;
    [SerializeField] private Vector2 _outlineEffectDistance;

    [SerializeField] private float _loopDelay;

    private Vector3 _originalScale;
    private Vector2 _originalShadowEffectDistance;
    private Vector2 _originalOutlineEffectDistance;
    private Shadow _shadow;
    private Outline _outline;

    private IEnumerator Start()
    {
        _shadow = GetComponent < Shadow >();
        _outline = GetComponent < Outline >();

        _originalScale = transform.localScale;

        DoScaleEffect();

        yield return new WaitForSeconds(_loopDelay);

        DoScaleEffect();
    }

    private void DoScaleEffect()
    {
        transform.DOScale(_scaleTo, _scaleToDuration).SetDelay(_scaleToDelay).OnComplete 
        (
            () =>
            {
                transform.DOScale(_originalScale, _scaleBackDuration).SetDelay(_scaleBackDelay);
            }
        );

        if (_shadow != null)
        {
            _originalShadowEffectDistance = _shadow.effectDistance;

            DOTween.To(() => _shadow.effectDistance, x => _shadow.effectDistance = x, _shadowEffectDistance, _scaleToDuration).SetDelay(_scaleToDelay).OnComplete
            (
                () =>
                {
                    DOTween.To(() => _shadow.effectDistance, x => _shadow.effectDistance = x, _originalShadowEffectDistance, _scaleBackDuration).SetDelay(_scaleBackDelay);
                }
            );
        }

        if (_outline != null)
        {
            _originalOutlineEffectDistance = _outline.effectDistance;

            DOTween.To(() => _outline.effectDistance, x => _outline.effectDistance = x, _outlineEffectDistance, _scaleToDuration).SetDelay(_scaleToDelay).OnComplete
            (
                () =>
                {
                    DOTween.To(() => _outline.effectDistance, x => _outline.effectDistance = x, _originalOutlineEffectDistance, _scaleBackDuration).SetDelay(_scaleBackDelay);
                }
            );

        }

      
    }

}
