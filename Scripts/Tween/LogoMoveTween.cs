﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LogoMoveTween : MonoBehaviour
{
    public float delay = 0f;
    public Vector3 scaleTo;
    public float scaleDuration = 1f;
    public float delayBeforeMovingIntoPosition;
    public Vector3 endPosition;
    public float moveDuration;

    private void Start()
    {
        Sequence sequence = DOTween.Sequence();

        sequence.AppendInterval(delay);
        sequence.Append(transform.DOScale(scaleTo, scaleDuration));
        sequence.AppendInterval(delayBeforeMovingIntoPosition);
        sequence.Append(transform.DOLocalMove(endPosition, moveDuration));
        sequence.Join(transform.DOScale(Vector3.one, moveDuration));
    }
}
