﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MoveInTween : MonoBehaviour
{
    [SerializeField] private float yOffsetPosition = 0f;
    public Vector3 moveFrom = new Vector3(217f, 0f, 0f);
    public float moveInTime;
    public float moveInDelay;

    public void Start()
    {
        moveFrom.y += yOffsetPosition;
        transform.DOLocalMove(moveFrom, moveInTime).SetDelay(moveInDelay).SetEase(Ease.OutBack);  
    }
}
