﻿using UnityEngine;
using System.Collections;

public class PhraseObject
{
    private string _phrase;

    public string Phrase
    {
        get { return _phrase; }
        private set
        {
            _phrase = value;
            LinePhrases = value.Split('|');
        }
    }
    public string Hint { get; private set; }
    public string[] LinePhrases { get; private set; }
    
    public PhraseObject(string phrase, string hint)
    {
        Phrase = phrase;
        Hint = hint;
    }
    
    // Use to get a starting line
    public int GetStartingLine()
    {
        if (LinePhrases.Length < 1 || LinePhrases.Length > 4)
        {
            Debug.LogError("[GetStartingLine] Number of lines : " + LinePhrases.Length);
            return -1;
        }

        Debug.Log("line count: " + LinePhrases.Length + " start at line number " + (2 - (LinePhrases.Length + 1) / 2));
        return 2 - (LinePhrases.Length + 1) / 2;
    }

    // Use to get a starting index
    public int GetStartingIndex()
    {
        var maxLength = 0;
        for (var i = 0; i < LinePhrases.Length; i++)
        {
            if (LinePhrases[i].Length > maxLength)
            {
                maxLength = LinePhrases[i].Length;
            }
        }

        // make the midpoint of the longest phrase = the midpoint of line
        // line length: 14 -> mid = index[6] / [7]
        return Mathf.FloorToInt((14 - maxLength )/ 2);
    }
}
