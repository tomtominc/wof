﻿using System;
using UnityEngine;

public class RewardItem : ISelectable
{
    public enum Color
    {
        White,
        Red,
        Lavender,
        Pink,
        Green,
        Orange,
        Yellow,
        Blue,
        Black,
        Variable,
        Bankrupt,
        LoseATurn,
        GrandPrize
    }

    public string TextInPie { get; private set; }

    public float PieAngular { get; private set; }

    public float Probability { get; private set; }

    public Color PieColor { get; private set; }

    public float AdjustedSelectionWeight { get; set; }

    public float GetSelectionWeight()
    {
        return Probability;
    }

    // RewardItem constructor
    public RewardItem(string textInPie, float pieAngular, string pieColor, float probability)
    {
        TextInPie = textInPie;
        PieAngular = pieAngular;
        Probability = probability;

        PieColor = (Color)Enum.Parse(typeof(Color), pieColor);
    }
}
