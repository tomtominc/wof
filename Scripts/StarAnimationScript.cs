﻿using UnityEngine;
using System.Collections;

public class StarAnimationScript : MonoBehaviour
{
    [SerializeField] private Animator _starAnimator;
    [SerializeField] private float _speed;
    [SerializeField] private bool _counterClockwise;

    // Use this for initialization
    void Start ()
	{
	    _starAnimator.speed = _speed;
        _starAnimator.SetBool("IsCounterClockwise", _counterClockwise);
	}
}
