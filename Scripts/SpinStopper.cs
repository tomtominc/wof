﻿using UnityEngine;
using System.Collections;

public class SpinStopper : MonoBehaviour
{

    private Vector3 _localPosition;

    // Use this for initialization
    void Start()
    {
        _localPosition = transform.localPosition;
    }
	
    // Update is called once per frame
    void Update()
    {
	    
        transform.localPosition = _localPosition;

    }

    void FixedUpdate()
    {
        transform.localPosition = _localPosition;
    }
}
