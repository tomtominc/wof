﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class SolvedAnimator : MonoBehaviour
{
    public Vector2 minMaskScale = new Vector2(0f, 1f);

    public RectTransform mask;
    public RectTransform shine;
    public float shineEffectX = 200f;
    public float maxHeight = 10f;
    public float maxShift = 38f;
    public float minScale = 0.5f;
    public float durationPart1 = 0.1f;
    public float durationPart2 = 0.1f;


    public float startDelay = 1f;
    public bool playOnAwake = true;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(startDelay);

        if (playOnAwake)
            DoAnimation();    
    }

    public void DoAnimation()
    {
        float totalTime = (transform.childCount + 1) * (durationPart1);
        shine.gameObject.SetActive(true);
        shine.DOMoveX(shineEffectX, totalTime).SetEase(Ease.Linear);
        mask.DOSizeDelta(minMaskScale, totalTime).SetEase(Ease.Linear);

        for (int i = 0; i < transform.childCount; i++)
        {
            RectTransform child = (RectTransform)transform.GetChild(i);

            float realDelay = durationPart1 * i;

            child.DOScaleX(minScale, durationPart1 + durationPart2).From().SetDelay(realDelay);
            child.DOMoveY(maxHeight, durationPart1).SetRelative(true).SetDelay(realDelay);

            child.DOMoveY(-maxHeight, durationPart2).SetRelative(true).SetDelay(durationPart1 + realDelay);

        }
       
    }
}
