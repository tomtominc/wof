﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Background : MonoBehaviour
{
    [SerializeField] private List<Animation> _animations = new List<Animation>();
    private int numberOfAnimationsUnfinished;

    public bool IsAllAnimationFinished { get; private set; }

    // Use this for initialization
    private void Start()
    {
        numberOfAnimationsUnfinished = _animations.Count;
        foreach (var animation in _animations)
        {
            StartCoroutine(WaitForAnimation(animation));
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (numberOfAnimationsUnfinished == 0 && !IsAllAnimationFinished)
        {
            IsAllAnimationFinished = true;
        }
    }

    private IEnumerator WaitForAnimation(Animation animation)
    {
        do
        {
            yield return null;
        } while (animation.isPlaying);
        numberOfAnimationsUnfinished--;
    }
}
