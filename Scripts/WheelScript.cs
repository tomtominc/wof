﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using DG.Tweening;
using LitJson;
using UnityEngine.UI;

public class WheelScript : MonoBehaviour
{
    public enum WheelStates
    {
        noSpin,
        spinning,
        spined,
        showingRewardPie,
        showedRewardPie,
        done
    }

    [SerializeField] private Transform _wheelTransform;
    [SerializeField] private Transform _referrenceZeroDegreeTransform;
    [SerializeField] private Transform _selectorTransform;
    [SerializeField] private Transform _wheelPieGroupTransform;
    [SerializeField] private GameObject _defaultPiePrefab;
    [SerializeField] private GameObject _3NumberPrefab;
    [SerializeField] private GameObject _4NumberPrefab;
    [SerializeField] private Sprite _bankruptSprite;
    [SerializeField] private Sprite _loseATurnSprite;
    [SerializeField] private Sprite _grandPrizeSprite;
    [SerializeField] private AudioClip _wheelSpinSlowSound;
    [SerializeField] private AudioClip _rewardReveal;
    [SerializeField] private AudioClip _flipSound;
    [SerializeField] private Transform _couponImage;
    [SerializeField] private float _flipAnimationDuration;
    [SerializeField] private Button _spinButton;

    private Rigidbody2D _wheelRigidbody;
    private List<RewardItem> _rewardItems;
    private AudioSource _audioSource;

    private List<GameObject> _pies = new List<GameObject>();
    private List<NumberInPie> _numberInPies = new List<NumberInPie>();
    private List<SpriteRenderer> _piesSpriteRenderers = new List<SpriteRenderer>();
    private float preAngle = 7.5f;
    private bool timeFlag = false;

    public WheelStates WheelState { get; private set; }

    public int RewardPieIndex { get; private set; }

    public float WheelAngularVelocity
    {
        get
        {
            return _wheelRigidbody.angularVelocity;
        }
        set
        {
            if (WheelState == WheelStates.noSpin)
            {
                _wheelRigidbody.angularVelocity = value;
                //SetSpinWheel(value,3.0f, RandomRewardpieIndex());
                WheelState = WheelStates.spinning;
            }
            else
            {
                Debug.LogError("[WheelAngularVelocity] Can't set WheelAngularVelocity because wheel is spined");
            }
        }
    }

    private float _sumProbability = 0.0f;
    private float _angularVelocity;
    private float _spinTimeCouter;

    private void Start()
    {
        _audioSource = GetComponent < AudioSource >();
        _wheelRigidbody = _wheelTransform.gameObject.GetComponent<Rigidbody2D>();


        RewardPieIndex = FullGameScene.Instance.RewardPieIndex;
        _couponImage.GetComponent < SpriteRenderer >().sprite = FullGameScene.Instance.CouponSprite;
    }

    //private int RewardPieIndex;
    public void SetSpinWheel(float angularVelocity, float leastTime)
    {
        SetSpinWheel(angularVelocity, leastTime, RewardPieIndex);
    }

    public void SetSpinWheel(float angularVelocity, float leastTime, int rewardIndex)
    {
        _angularVelocity = angularVelocity;
        _spinTimeCouter = leastTime;
        RewardPieIndex = rewardIndex;
        WheelState = WheelStates.spinning;
        timeFlag = true;
    }

    public int RandomRewardpieIndex()
    {
        Selector selector = new Selector();
        RewardItem rewardItem = selector.Single(_rewardItems);
        return _rewardItems.IndexOf(rewardItem);
    }

    /// <summary>
    /// Use to set reward items in wheel
    /// </summary>
    public void SetupWheelWithRewardItems(List<RewardItem> rewardItems)
    {
        _rewardItems = new List <RewardItem>(rewardItems);
    }

    /// <summary>
    /// Use to check parameter value and gen pie in wheel
    /// </summary>
    public void InitWheel()
    {
        var sumAngular = 0.0f;
        for (var i = 0; i < _rewardItems.Count; i++)
        {
            if (_rewardItems[i].PieAngular < 0)
            {
                Debug.LogError("[InitWheel()] PieAngular at index " + i + " is  < 0");
                return;
            }
            _sumProbability += _rewardItems[i].Probability;
            sumAngular += _rewardItems[i].PieAngular;
        }
        if (sumAngular < 360)
        {
            Debug.LogError("[InitWheel()] Sum of PieAngular < 360");
            return;
        }
        if (sumAngular > 360)
        {
            Debug.LogError("[InitWheel()] Sum of PieAngular > 360");
            return;
        }

        _piesSpriteRenderers.Clear();
        sumAngular = 0.0f;
        for (var i = 0; i < _rewardItems.Count; i++)
        {
            //_pies.Add(Instantiate(_defaultPiePrefab) as GameObject);
            _pies.Add(Instantiate(_defaultPiePrefab, _wheelTransform.position, Quaternion.identity) as GameObject);
            _pies[i].name = "WheelPie" + i;
            _pies[i].transform.SetParent(_wheelPieGroupTransform.transform);
            _piesSpriteRenderers.Add(_pies[i].GetComponent<SpriteRenderer>());
            

            if (_rewardItems[i].TextInPie.Length == 3)
            {
                _numberInPies.Add((Instantiate(_3NumberPrefab, _wheelTransform.position, Quaternion.identity) as GameObject).GetComponent<NumberInPie>());
                _numberInPies[i].gameObject.transform.SetParent(_pies[i].transform);
                _numberInPies[i].transform.localScale = new Vector3(1, 1, 1);
                _numberInPies[i].SetNumber(_rewardItems[i].TextInPie);
            }
            else if (_rewardItems[i].TextInPie.Length == 4)
            {
                _numberInPies.Add((Instantiate(_4NumberPrefab, _wheelTransform.position, Quaternion.identity) as GameObject).GetComponent<NumberInPie>());
                _numberInPies[i].gameObject.transform.SetParent(_pies[i].transform);
                _numberInPies[i].transform.localScale = new Vector3(1, 1, 1);
                _numberInPies[i].SetNumber(_rewardItems[i].TextInPie);
            }
            else
            {
                _numberInPies.Add(null);
            }

            // Set Color
            SetPieColorByRewardItem(_pies[i], _numberInPies[i], _rewardItems[i]);
            
            _pies[i].transform.localScale = new Vector3(1, 1, 1);
            _pies[i].transform.localEulerAngles = new Vector3(0, 0, sumAngular + _rewardItems[i].PieAngular / 2);
            sumAngular += _rewardItems[i].PieAngular;
        }
    }

    /// <summary>
    /// Use to set color of each pie and its shadow
    /// </summary>
    private void SetPieColorByRewardItem(GameObject pie, NumberInPie numberGroup, RewardItem item)
    {
        // set RGB value
        byte red, green, blue, alpha;
        byte shadowRed, shadowGreen, shadowBlue, shadowAlpha;

        alpha = 255;
        shadowAlpha = 255;

        SpriteRenderer pieSpriteRenderer = pie.GetComponent<SpriteRenderer>();

        if (item.PieColor == RewardItem.Color.Bankrupt)
        {
            pieSpriteRenderer.sprite = _bankruptSprite;
        }
        else if (item.PieColor == RewardItem.Color.LoseATurn)
        {
            pieSpriteRenderer.sprite = _loseATurnSprite;
        }
        else if (item.PieColor == RewardItem.Color.GrandPrize)
        {
            pieSpriteRenderer.sprite = _grandPrizeSprite;
        }
        else
        {
            switch (item.PieColor)
            {
                case RewardItem.Color.White:
                    red = 237;
                    green = 242;
                    blue = 234;

                    shadowRed = 152;
                    shadowGreen = 151;
                    shadowBlue = 155;

                    break;

                case RewardItem.Color.Red:
                    red = 225;
                    green = 95;
                    blue = 85;

                    shadowRed = 143;
                    shadowGreen = 86;
                    shadowBlue = 82;

                    break;

                case RewardItem.Color.Lavender:
                    red = 164;
                    green = 126;
                    blue = 198;

                    shadowRed = 122;
                    shadowGreen = 88;
                    shadowBlue = 150;
                    break;

                case RewardItem.Color.Pink:
                    red = 254;
                    green = 153;
                    blue = 168;

                    shadowRed = 216;
                    shadowGreen = 121;
                    shadowBlue = 135;
                    break;

                case RewardItem.Color.Green:
                    red = 0;
                    green = 178;
                    blue = 122;

                    shadowRed = 52;
                    shadowGreen = 121;
                    shadowBlue = 99;
                    break;

                case RewardItem.Color.Orange:
                    red = 255;
                    green = 108;
                    blue = 47;

                    shadowRed = 171;
                    shadowGreen = 97;
                    shadowBlue = 74;
                    break;

                case RewardItem.Color.Yellow:
                    red = 255;
                    green = 236;
                    blue = 45;

                    shadowRed = 166;
                    shadowGreen = 129;
                    shadowBlue = 61;
                    break;

                case RewardItem.Color.Blue:
                    red = 76;
                    green = 180;
                    blue = 231;

                    shadowRed = 33;
                    shadowGreen = 106;
                    shadowBlue = 149;
                    break;

                case RewardItem.Color.Black:
                    red = 35;
                    green = 31;
                    blue = 32;

                    shadowRed = 143;
                    shadowGreen = 86;
                    shadowBlue = 82;
                    break;

                case RewardItem.Color.Variable:
                    red = green = blue = alpha = 255;

                    shadowRed = 143;
                    shadowGreen = 86;
                    shadowBlue = 82;
                    break;

                default:
                    red = green = blue = alpha = 255;
                    shadowRed = shadowGreen = shadowBlue = shadowAlpha = 255;

                    break;
            }

            Color32 newColor = new Color32(red, green, blue, alpha);
            Color32 shadowColor = new Color32(0, 0, 0, 200);  //new Color32(shadowRed, shadowGreen, shadowBlue, shadowAlpha);

            pieSpriteRenderer.color = newColor;

            if (numberGroup != null)
            {
                foreach (Transform number in numberGroup.transform)
                {
                    SpriteRenderer shadow = number.GetChild(0).GetComponent<SpriteRenderer>();

                    shadow.color = shadowColor;

                }
            }
            // change color of each shadow
        }
    }

    bool playedRewardRevealedSound = false;

    public void PlayRewardPieSFX()
    {
        if (!playedRewardRevealedSound)
        {
            AudioSource.PlayClipAtPoint(_rewardReveal, Camera.main.transform.position, 0.5f);
            playedRewardRevealedSound = true;
        }
      
        //_audioSource.PlayOneShot(_rewardReveal, 1.0f);
    }

    /// <summary>
    /// Use to show reward pie with animation
    /// </summary>
    public void ShowRewardPie()
    {
        WheelState = WheelStates.showingRewardPie;
        _pies[RewardPieIndex].transform.SetParent(null, false);
        _piesSpriteRenderers[RewardPieIndex].sortingOrder = 10;
        if (_numberInPies[RewardPieIndex] != null)
            _numberInPies[RewardPieIndex].SetSortingOrder(12);
        _pies[RewardPieIndex].GetComponent<Animation>().Play();
    }

    /// <summary>
    /// Use to destroy reward pie
    /// </summary>
    public void DestroyRewardPie()
    {
        Destroy(_pies[RewardPieIndex]);
    }

    private bool _signForStopWheel;
    private float timer = 0.0f;

    private void Update()
    {
        if (timeFlag)
        {
            timer += Time.deltaTime;
        }
        if (!timeFlag && timer != 0)
        {
//			Debug.Log ("time is: "+timer);
            timer = 0.0f;
        }
        if (WheelState == WheelStates.spinning)
        {
            if (Mathf.Abs(_wheelTransform.localEulerAngles.z - preAngle) >= 15.0f)
            {
                //Debug.LogFormat("Playing audio: {0}", name);
                //AudioSource.PlayClipAtPoint(_wheelSpinSlowSound, GameObject.Find("Main Camera").gameObject.transform.position);
                _audioSource.PlayOneShot(_wheelSpinSlowSound, 3.0f);
                preAngle = _wheelTransform.localEulerAngles.z;
            }
            if (_spinTimeCouter >= 0)
            {
                _wheelRigidbody.angularVelocity = _angularVelocity;
                _spinTimeCouter -= Time.deltaTime;
            }
            else if (_signForStopWheel)
            {
                if (Math.Abs(_wheelTransform.localEulerAngles.z - (360 - (RewardPieIndex * 15) - 7.5f)) < 2.5f)
                {
                    _wheelTransform.localEulerAngles = new Vector3(_wheelTransform.localEulerAngles.x, _wheelTransform.localEulerAngles.y, 360 - (RewardPieIndex * 15) - 7.5f);
                    _wheelRigidbody.angularVelocity = 0.0f;
                    WheelState = WheelStates.spined;
                    Debug.Log("SPINED");
                    timeFlag = false;
                }
                else
                {
                    if (Math.Abs(_wheelRigidbody.angularVelocity) < 70.0f)
                    {
                        if ((Math.Abs(_wheelTransform.localEulerAngles.z - (360 - RewardPieIndex * 15 - 7.5)) < 90.0f) && ((_wheelTransform.localEulerAngles.z - (360 - RewardPieIndex * 15 - 7.5)) > 0))
                        {
                            _wheelRigidbody.angularDrag = 0.15f;
                        }
                        else
                        {
                            _wheelRigidbody.angularVelocity = -70.0f;
                        }
                    }
                }
            }
            else
            {
                if (!_signForStopWheel && Math.Abs(_wheelRigidbody.angularVelocity) < 180.0f && Math.Abs(_wheelTransform.localEulerAngles.z - (360 - RewardPieIndex * 15 - 7.5)) < 6)
                {
                    _wheelRigidbody.angularDrag = 5.5f;
                    _signForStopWheel = true;
                }
            }
        }
        else if (WheelState == WheelStates.showingRewardPie)
        {
            StartCoroutine(WaitForSecondsAndDoFunction(1.0f, () =>
                    {
                        WheelState = WheelStates.showedRewardPie;
                    }));
        }
        else if (WheelState == WheelStates.showedRewardPie)
        {
            // Debug.Log("ABCDEFG");
        }
    }

    public void TurnWedge()
    {
        Transform rewardPie = _pies[RewardPieIndex].transform;

        rewardPie.DOScaleX(0f, _flipAnimationDuration).OnComplete 
        (
            () =>
            {
                _couponImage.DOScaleX(20f, _flipAnimationDuration).SetEase(Ease.OutBack);
            }
        );
    }

    public IEnumerator TurnWedgeAround()
    {
        //_audioSource.PlayOneShot(_flipSound, 1.0f);

        Transform rewardPie = _pies[RewardPieIndex].GetComponent<Transform>();
        float targetScale = (-1) * (rewardPie.localScale.x);
        float scaleRate = 150;

        bool isFlipped = false;

        while (rewardPie.localScale.x >= targetScale)
        {
            rewardPie.localScale -= new Vector3(scaleRate, 0, 0) * Time.deltaTime;

            if (rewardPie.localScale.x <= 0 && !isFlipped)
            {
                isFlipped = true;

                // Destroy Numbers
                GameObject.Destroy(rewardPie.gameObject.GetComponentInChildren<NumberInPie>().gameObject);
                // Create a Picture HERE
            }

            if (rewardPie.localScale.x <= targetScale)
            {
                rewardPie.localScale = new Vector3(targetScale, (-1) * targetScale, 1);
            }

            yield return null;
        }

        yield return null;
    }

    private IEnumerator WaitForSecondsAndDoFunction(float waitTime, Action actionFunction)
    {
        yield return new WaitForSeconds(waitTime);
        actionFunction();
    }
}
