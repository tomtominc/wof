﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayBlockScript : MonoBehaviour
{
    [SerializeField] private Font _userFont;
    [SerializeField] private Image _cursorLight;
    [SerializeField] private Sprite _openedBlockSprite;
    [SerializeField] private Sprite _closedBlockSprite;
    [SerializeField] private Sprite _redLightSprite;
    [SerializeField] private Sprite _orangeLightSprite;
    [SerializeField] private Sprite _blueLightSprite;
    [SerializeField] private Text _displayText;
    [SerializeField] private Image _displayImage;
    private Vector3 _displayTextScale = new Vector3(0.4f, 0.4f, 1f);
    private Color _whiteColor = new Color(1, 1, 1, 1);
    private Color _cursorColor = new Color(0, 0, 1, 1);

    private float cursorFlashingTime = 0.75f;
    private float flashingTimer;

    public char AnswerChar { get; private set; }

    public char DisplayChar { get; private set; }

    public bool IsOpened { get; private set; }

    public bool IsCursorOn { get; private set; }

    // Use this for initialization
    private void Awake()
    {
        IsOpened = false;
        _displayText.rectTransform.localScale = _displayTextScale;
        _displayText.rectTransform.localPosition = new Vector2(0f, -20f);
        _displayText.font = _userFont;
    }


    private void Update()
    {
        if (IsCursorOn)
        {
            flashingTimer -= Time.deltaTime;
            if (_displayImage.color == _whiteColor)
            {
                if (flashingTimer <= 0)
                {
                    _displayImage.color = _cursorColor;
                    flashingTimer = cursorFlashingTime;
                }
            }
            if (_displayImage.color == _cursorColor)
            {
                if (flashingTimer <= 0)
                {
                    _displayImage.color = _whiteColor;
                    flashingTimer = cursorFlashingTime;
                }
            }
        }
    }

    /// <summary>
    /// Use to assign a answer letter to this block
    /// </summary>
    public void AssignAnswerLetter(char input)
    {
        AnswerChar = input;
        DisplayChar = ' ';

        if (input == ' ')
        {
            IsOpened = false;
        }
        else
        {
            IsOpened = true;
        }
    }

    /// <summary>
    /// Use to assign a display letter in this block
    /// </summary>
    public void AssignShowLetter(char input)
    {
        DisplayChar = input;
    }

    /// <summary>
    /// Use to set sprite block to open and show text in this block
    /// </summary>
    public void OpenThisBlock()
    {
        UpdateDisplayText(DisplayChar);
        _displayImage.sprite = _openedBlockSprite;
    }

    /// <summary>
    /// Use to set sprite block to close and hide text in this block
    /// </summary>
    public void CloseThisBlock()
    {
        UpdateDisplayText(' ');
        _displayImage.sprite = _closedBlockSprite;
    }

    public void SetCursorLight()
    {
        IsCursorOn = true;
        flashingTimer = cursorFlashingTime;
        _displayImage.color = _cursorColor;
    }

    /// <summary>
    /// Use to set light color to red
    /// </summary>
    public void SetLightRed()
    {
        _cursorLight.sprite = _redLightSprite;
    }

    public void SetLightOrange()
    {
        _cursorLight.sprite = _orangeLightSprite;
    }

    /// <summary>
    /// Use to set light color to blue
    /// </summary>
    public void SetLightBlue()
    {
        _cursorLight.sprite = _blueLightSprite;
    }

    /// <summary>
    /// Use to light this block
    /// </summary>
    public void Light()
    {
        _cursorLight.gameObject.SetActive(true);
    }

    /// <summary>
    /// Use to unlight this block
    /// </summary>
    public void Unlight()
    {
        IsCursorOn = false;
        _displayImage.color = _whiteColor;
        _cursorLight.gameObject.SetActive(false);
    }

    /// <summary>
    /// Use to show answer letter with red color
    /// </summary>
    public void ShowAnswer()
    {
        UpdateDisplayText(AnswerChar);
        _displayText.color = Color.red;
    }


    /// <summary>
    /// Use to update display text
    /// </summary>
    private void UpdateDisplayText(char text)
    {
//        if (text.Equals('W'))
//            _displayText.rectTransform.localScale = new Vector3(0.28f, 0.4f, 1f);
//        else
//            _displayText.rectTransform.localScale = _displayTextScale;
        
        _displayText.text = text.ToString();
    }
}
