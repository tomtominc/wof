﻿using UnityEngine;
using System.Text;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using System;

public class HintPanelDisplay : MonoBehaviour
{
    [SerializeField] private Text _hintText;
    // [SerializeField] private Text _revealedCharText;
    [SerializeField] private List<Text> _revealedCharDisplays = new List<Text>();
    [SerializeField] private Text _IntroText;

    [SerializeField] private Sprite _congratsHelpSprite;
    [SerializeField] private Image _playIntroPanel;
    [SerializeField] private Image _overlayIntroPanel;
    [SerializeField] private GameObject _selectedLettersMask;

    [SerializeField] private Vector2 _instructionPoint1;
    [SerializeField] private Vector2 _instructionPoint2;
    [SerializeField] private float _instructionMoveDelay = 3.0f;


    public void SetRevealedCharAt(int boxIndex, string pickedChar)
    {
        _revealedCharDisplays[boxIndex].GetComponent<Text>().text = pickedChar;
    }

    // Use to set hint text
    public void SetHintText(string text)
    {
        _hintText.text = text;
    }

    // Use to set intro text
    public void SetIntroText(string text)
    {
        _IntroText.text = text;
    }

    public void SetCongratulationPanel()
    {
        _playIntroPanel.sprite = _congratsHelpSprite;
        _IntroText.text = "";
    }

    public void SetBoxDisable()
    {
        _selectedLettersMask.SetActive(false);
    }

    public void SetBoxEnable()
    {
        _selectedLettersMask.SetActive(true);
    }

    public void DoHintAnimationInNonInvasive(Action onComplete)
    {
        Debug.Log("hello?");
        _playIntroPanel.DOFade(1f, 0.3f);
        _IntroText.gameObject.SetActive(true);
        _playIntroPanel.transform.localPosition = new Vector3(1048f, _instructionPoint2.y);
        _playIntroPanel.transform.DOLocalMove(_instructionPoint2, 0.3f).OnComplete(() =>
            {
                if (onComplete != null)
                    onComplete.Invoke();
            });
    }

    public void DoHintAnimationIn(Action onComplete)
    {
        
        _playIntroPanel.transform.localPosition = new Vector3(1048f, _instructionPoint1.y);

        Sequence _animationIn = DOTween.Sequence();


        Tweener move1 = _playIntroPanel.transform.DOLocalMove(_instructionPoint1, 0.3f);
        Tweener color = _playIntroPanel.DOFade(1f, 0.3f);

        _animationIn.Append(move1);
        _animationIn.Insert(0f, color);
        _animationIn.AppendCallback(() =>
            {
                _IntroText.gameObject.SetActive(true);
            });
        _animationIn.AppendInterval(_instructionMoveDelay);

        Tweener move2 = _playIntroPanel.transform.DOLocalMove(_instructionPoint2, 0.3f);

        _animationIn.Append(move2);

        _animationIn.OnComplete(() =>
            {
                onComplete.Invoke();
            });

    }

    public void DoHintAnimationOut(Action onComplete)
    {
        _playIntroPanel.transform.DOLocalMove(new Vector2(-1048f, _instructionPoint2.y), 0.3f);

        _playIntroPanel.DOFade(0f, 0.3f).OnComplete(() =>
            {
                _IntroText.gameObject.SetActive(false);
                onComplete.Invoke();
            });
    }

    /*
    // Use to set revealed char text
    public void SetRevealedCharText(string text)
    {
        var tempRevealedCharText = "";
        for (var i = 0; i < text.Length; i++)
        {
            if (i == 0) tempRevealedCharText = text[i].ToString();
            else tempRevealedCharText += " "+ text[i];
        }
        _revealedCharText.text = text;
    }
    
    // Use to add revealed char text with string
    public void AddRevealedCharText(string text)
    {
        foreach (var character in text)
        {
            AddRevealedCharText(character);
        }
    }

    // Use to add revealed char text with character
    public void AddRevealedCharText(char character)
    {
        var index = _revealedCharText.text.IndexOf('_');
        if (index == -1)
        {
            Debug.LogError("[AddRevealedCharText] Don't have '_' in revealedCharText");
            return;
        }
        StringBuilder tempStringBuilder = new StringBuilder(_revealedCharText.text);
        tempStringBuilder[index] = character;
        _revealedCharText.text = tempStringBuilder.ToString();
    }

    */
}
