﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TransitionLight : MonoBehaviour
{
    [SerializeField] private Image _transitionLight;
    [SerializeField] private List<Image> _starsInWave1 = new List<Image>();
    [SerializeField] private List<Image> _starsInWave2 = new List<Image>();
    private float _timeCounter;
    private Dictionary<Image, float[]> _fadeInTimeDictionary = new Dictionary<Image, float[]>();
    private Dictionary<Image, float[]> _fadeOutTimeDictionary = new Dictionary<Image, float[]>();
    private float _lastAnimationTime;
    private TransitionLightStates transitionLightState;
    private enum TransitionLightStates
    {
        animateTransition,transitionEnd
    }
    
    // Use this for initialization
    private void Start ()
    {
        _timeCounter = 0;
        _lastAnimationTime = 0.0f;
        
        _transitionLight.color = new Color(1, 1, 1, 0);
        _fadeInTimeDictionary.Add(_transitionLight, new[] { 0.15f, 0.2f });
        _fadeOutTimeDictionary.Add(_transitionLight, new[] { 0.85f, 1.15f });

        foreach (var star in _starsInWave1)
        {
            star.color = new Color(1, 1, 1, 0);
            _fadeInTimeDictionary.Add(star, new[] { 0.0f, 1.0f });
        }
        foreach (var star in _starsInWave2)
        {
            star.color = new Color(1, 1, 1, 0);
            _fadeInTimeDictionary.Add(star, new[] { 1.0f, 1.15f });
            _fadeOutTimeDictionary.Add(star, new[] { 0.85f, 1.15f });
        }

        foreach (var animationTime in _fadeInTimeDictionary.Values)
        {
            if (animationTime[1] > _lastAnimationTime)
            {
                _lastAnimationTime = animationTime[1];
            }
        }
        foreach (var animationTime in _fadeOutTimeDictionary.Values)
        {
            if (animationTime[1] > _lastAnimationTime)
            {
                _lastAnimationTime = animationTime[1];
            }
        }

        transitionLightState = TransitionLightStates.animateTransition;
    }
	
	// Update is called once per frame
	private void Update ()
    {
        if (transitionLightState == TransitionLightStates.animateTransition)
        {
            if (_timeCounter < _lastAnimationTime + 0.5f)
            {
                _timeCounter += Time.deltaTime;

                foreach (var image in _fadeInTimeDictionary.Keys)
                {
                    var time = _fadeInTimeDictionary[image][1] - _fadeInTimeDictionary[image][0];
                    if (image.color.a < 1 && _timeCounter > _fadeInTimeDictionary[image][0] && _timeCounter <= _fadeInTimeDictionary[image][1])
                    {

                        image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a + Time.deltaTime / time);
                    }
                }
                foreach (var image in _fadeOutTimeDictionary.Keys)
                {
                    var time = _fadeOutTimeDictionary[image][1] - _fadeOutTimeDictionary[image][0];
                    if (image.color.a > 0 && _timeCounter > _fadeOutTimeDictionary[image][0])
                    {

                        image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - Time.deltaTime / time);
                    }
                }
            }
            else
            {
                transitionLightState = TransitionLightStates.transitionEnd;
            }
        }
	    if (transitionLightState == TransitionLightStates.transitionEnd)
	    {
	        Destroy(gameObject);
	    }
    }
}
