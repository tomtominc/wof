﻿using UnityEngine;
using System.Collections;

public class SelectorAudioController : MonoBehaviour
{

    [SerializeField] private AudioClip _wheelSpinSlowSound;
    private string preStickName = "";

    void Start()
    {
	
    }

    void Update()
    {
        
	
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.LogFormat("Playing audio: {0}", name);
        AudioSource.PlayClipAtPoint(_wheelSpinSlowSound, GameObject.Find("Main Camera").gameObject.transform.position);
    }


}
