﻿using UnityEngine;
using System.Collections;

public class SportLightAnimation : MonoBehaviour
{

    [SerializeField] private Animator _sportLightAnimator;
    [SerializeField] private float _speed;

    // Use this for initialization
    void Start ()
    {
        _sportLightAnimator.speed = _speed;
    }
}
