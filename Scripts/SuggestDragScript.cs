﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SuggestDragScript : MonoBehaviour {

    public SpriteRenderer arrowDown;           // Sprite of suggestion arrow
    public SpriteRenderer pointer;              // Sprite of suggestion pointer

    private Vector3 _PointerFirstPosition;      // FirstPosition that get from pointer
    private float _PointerMoveRangeY = -50.0f;  // Range that pointer will be move and it will move in 1 sec

    private float _AnimetionTime = 0.0f;        // Time counter parameter that use to count animetion time
    private float _FadeSpeed = 1.5f;            // Fade speed is alpha range that will be change per deltaTime

    private int _State;                         // State of suggestion ( 0->fade in, 1->move down, 2->hold, 3->fade out, 4->hold and reset position, 5->hide suggest )

    // Use this for initialization
    void Start()
    {
        // Get first position of suggestion pointer and set state to start fade in
        _PointerFirstPosition = pointer.GetComponent<Transform>().position;
        _State = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _AnimetionTime += Time.deltaTime;

        // State 0 is fate in state
        if (_State == 0)
        {
            // Set new alpha to suggestion arrow ( ArrowDown ) and suggestion pointer ( _Pointer )
            arrowDown.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, _AnimetionTime * _FadeSpeed);
            pointer.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, _AnimetionTime * _FadeSpeed);

            // Change state and reset time counter when arrow finished appear (alpha >= 1)
            if (pointer.GetComponent<SpriteRenderer>().color.a >= 1)
            {
                _State = 1;
                _AnimetionTime = 0.0f;
            }
        }

        // State 1 is move down state
        else if (_State == 1)
        {
            // Set new suggestion pointer ( _Pointer ) position 
            pointer.GetComponent<Transform>().position = new Vector3(_PointerFirstPosition.x, _PointerFirstPosition.y + (_AnimetionTime * _PointerMoveRangeY), _PointerFirstPosition.z);

            // Change state and reset time counter when arrow finished move down ( move range <= that you want)
            if ((_AnimetionTime * _PointerMoveRangeY) <= _PointerMoveRangeY)
            {
                _State = 2;
                _AnimetionTime = 0.0f;
            }
        }

        // State 2 is hold state
        else if (_State == 2)
        {
            // Change state and reset time counter when 0.5sec pass
            if (_AnimetionTime >= 0.5f)
            {
                _State = 3;
                _AnimetionTime = 0.0f;
            }
        }

        // State 3 is fade out state
        else if (_State == 3)
        {
            // Set new alpha to suggestion arrow ( ArrowDown ) and suggestion pointer ( _Pointer )
            arrowDown.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f - (_AnimetionTime * _FadeSpeed));
            pointer.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f - (_AnimetionTime * _FadeSpeed));

            // Change state and reset time counter when arrow finished disappear (alpha <= 0)
            if (pointer.GetComponent<SpriteRenderer>().color.a <= 0)
            {
                _State = 4;
                _AnimetionTime = 0.0f;
            }
        }

        // State 4 is hold state
        else if (_State == 4)
        {
            // Change state and reset time counter when 0.5sec pass and then reset the arrow position
            if (_AnimetionTime >= 0.5f)
            {
                _State = 0;
                _AnimetionTime = 0.0f;
                pointer.GetComponent<Transform>().position = _PointerFirstPosition;
            }
        }

        // State 5 is hide state ( fade out and hide it )
        else if (_State == 5)
        {
            // Continuous fade out if suggestion is not disappear ( alpha>=0 )
            if (pointer.GetComponent<SpriteRenderer>().color.a >= 0)
            {
                arrowDown.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f - (_AnimetionTime * _FadeSpeed));
                pointer.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f - (_AnimetionTime * _FadeSpeed));
            }
        }
    }

    //Use this for show suggest agian ( change to state 4 )
    public void ShowSuggestArrow()
    {
        _State = 4;
        _AnimetionTime = 0.0f;
    }
    
    //Use this for hide suggetion pointer and arrow ( change to state 5 )
    public void HideSuggestArrow()
    {
        _State = 5;
    }
}
