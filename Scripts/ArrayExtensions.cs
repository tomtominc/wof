﻿using UnityEngine;
using System.Collections;

public static class ArrayExtensions {

    // Use this to fill any array with a value
    public static void Fill<T>(this T[] originalArray, T with)
    {
        for (int i = 0; i < originalArray.Length; i++)
        {
            originalArray[i] = with;
        }
    }
}
