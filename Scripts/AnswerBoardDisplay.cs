﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class AnswerBoardDisplay : MonoBehaviour
{
    [SerializeField]
    private List<DisplayLineScript> _displayLines = new List<DisplayLineScript>();
    private string _inputPhrase;
    private int _startLine;
    private int _startIndex;
    private int _cursorLineIndex;
    private int _cursorIndex;

    public bool isDisplayInitialized;

    public int LineCount
    {
        get
        {
            int count = 0;

            for (int i = _startLine; i < 4; i++)
            {
                if (_displayLines[i].IsAssigned)
                    count++;
                else
                    break;
            }

            return count;
        }
    }

    /// <summary>
    /// Use to initialize all attribute needs to used in the displays
    /// </summary>
    public void Init(PhraseObject inputPhrase)
    {
        isDisplayInitialized = false;

        _startIndex = inputPhrase.GetStartingIndex();
        _startLine = inputPhrase.GetStartingLine();
        _cursorIndex = _startIndex;
        _cursorLineIndex = _startLine;

        var maxBlocksInDisplayLine = 0;
        for (var i = _startLine; i < _displayLines.Count; i++)
        {
            var blocksInDisplayLine = _displayLines[i].GetNumOfBlocks();
            if (maxBlocksInDisplayLine < blocksInDisplayLine)
            {
                maxBlocksInDisplayLine = blocksInDisplayLine;
            }
        }

        for (var i = _startLine; i < _displayLines.Count; i++)
        {
            var startIndex = _startIndex - (maxBlocksInDisplayLine - _displayLines[i].GetNumOfBlocks()) / 2;
            if (startIndex < 0)
            {
                startIndex = 0;
            }
            _displayLines[i].StartIndex = startIndex;
        }

        var tempLineCursor = _startLine;
        foreach (string phrase in inputPhrase.LinePhrases)
        {
            _displayLines[tempLineCursor].AssignAnswerPhraseInLine(phrase);

            // COROUTINE AND WAIT UNTIL THIS FINISH 
            // _displayLines[tempLineCursor].RevealPuzzleLine();
            tempLineCursor++;
        }

        StartCoroutine(RevealPuzzleLines());
    }

    public IEnumerator RevealPuzzleLines()
    {
        var tempLineCursor = _startLine;

        foreach (DisplayLineScript line in _displayLines)
        {
            StartCoroutine(line.RevealPuzzleLine());
            yield return new WaitForSeconds(0.5f);
        }

        isDisplayInitialized = true;

        yield return null;
    }

    /// <summary>
    /// Use to update display board and show cursor
    /// </summary>
    public void UpdateDisplayBoard(string inputPhrase, int cursorIndex)
    {
        UpdateDisplayBoard(inputPhrase, cursorIndex, true);
    }

    /// <summary>
    /// Use to update display board
    /// </summary>
    public void UpdateDisplayBoard(string inputPhrase, int cursorIndex, bool isCursorShown)
    {
        // Update text
        var linePhrases = inputPhrase.Split('|');
        var tempLineCursor = _startLine;
        foreach (var phrase in linePhrases)
        {
            _displayLines[tempLineCursor].AssignShowPhraseInLine(phrase);
            tempLineCursor++;
        }
        DisplayAllLines();

        // Update cursor
        _cursorLineIndex = _startLine;
        _cursorIndex = _startIndex;
        foreach (var linePhrase in linePhrases)
        {
            if (linePhrase.Length < cursorIndex)
            {
                _cursorLineIndex++;
                cursorIndex -= linePhrase.Length + 1;
            }
            else
            {
                _cursorIndex = _displayLines[_cursorLineIndex].StartIndex + cursorIndex;
                break;
            }
        }/*
        if (isCursorShown)
        {
            ShowCursor();
        }
        else
        {
            HideCursor();
        }*/
    }

    /// <summary>
    /// Use to show cursor in this board
    /// </summary>
    public void ShowCursor()
    {
        HideCursor();
        _displayLines[_cursorLineIndex].LightCursorAtIndex(_cursorIndex);
    }

    /// <summary>
    /// Use to hide sursor in this board
    /// </summary>
    public void HideCursor()
    {
        foreach (var displayLine in _displayLines)
        {
            displayLine.UnlightCursorAllBlocksInLine();
        }
    }

    /// <summary>
    /// Use to 
    /// </summary>
    public void LightEmptyAllLines(bool isWrongAnswer = false)
    {
        for (int i = 0; i < _displayLines.Count; i++)
        {
            _displayLines[i].SetLightEmptyInLine(isWrongAnswer);
            if (i == _cursorLineIndex)
                _displayLines[i].SetCursorInLine(_cursorIndex - _displayLines[i].StartIndex);
        }
        /*foreach (var displayLine in _displayLines)
        {
            displayLine.SetLightEmptyInLine(isWrongAnswer);
        }*/
    }

    /// <summary>
    /// Use to display all lines in this board
    /// </summary>
    public void DisplayAllLines()
    {
        foreach (var displayLine in _displayLines)
        {
            displayLine.DisplayThisLine();
        }
    }

    /// <summary>
    /// Use to undisplay all lines in this board
    /// </summary>
    public void UndisplayAllLines()
    {
        foreach (var displayLine in _displayLines)
        {
            displayLine.UndisplayThisLine();
        }
    }

    /// <summary>
    /// Use to show answer in this board
    /// </summary>
    public void ShowAnswerInBoard()
    {
        foreach (var displayLine in _displayLines)
        {
            displayLine.ShowAnswerInLine();
        }
    }
}
