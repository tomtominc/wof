﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour
{
    public GameObject nextPrefab;

    // Use this for Load (fade out out page & fade in new page)
    public void loadScene(string destroyPrefabName)
    {
        // Instantiate next page's prefab and call fade in function
        GameObject pageCreate = Instantiate(nextPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        StartCoroutine(FadePageIn(pageCreate));

        // Get page object that you want to fade out and destroy it then call function to do that
        GameObject pageDelete = GameObject.Find(destroyPrefabName + "(Clone)");
        StartCoroutine(FadePageOut(pageDelete));
    }


    //Use this for fade in new page that is canvas and have CanvasGroup Componenet
    IEnumerator FadePageIn(GameObject pageCreate)
    {
        CanvasGroup fadeCanvas = pageCreate.GetComponent<CanvasGroup>();

        Debug.LogFormat("Fade Canvas: {0}", fadeCanvas);
        
        //Add alpha by deltatime to fade in new page
        while (fadeCanvas.alpha < 1)
        {
            fadeCanvas.alpha += Time.deltaTime;
            yield return null;
        }

        //Set enable page
        fadeCanvas.interactable = true;
        yield return null;
    }
    
    //Use this for fade out page that is canvas and have CanvasGroup Componenet
    IEnumerator FadePageOut(GameObject pageDelete)
    {
        CanvasGroup fadeCanvas = pageDelete.GetComponent<CanvasGroup>();

        //Set disable page
        fadeCanvas.interactable = false;
        
        //Subtract alpha by deltatime to fade out old page
        while (fadeCanvas.alpha > 0)
        {
            fadeCanvas.alpha -= Time.deltaTime;
            yield return null;
        }

        //Destroy it
        Destroy(pageDelete);
        yield return null;
    }
}
