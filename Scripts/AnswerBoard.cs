﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System;

public class AnswerBoard : MonoBehaviour
{
    private static readonly string _splitLineCharacter = "|";
    private static readonly string _defaultRevealedCharString = "RSTLNE";
    private static readonly int _numOfRevealCharacter = 10;
    private static readonly string _backspaceKeyValue = "Backspace";

    [SerializeField] private AnswerBoardDisplay _displayBoard;
    [SerializeField] private HintPanelDisplay _hintPanelDisplay;


    [SerializeField] private AudioClip _pingSound;

    private string _phrase;
    private string _revealedCharString;
    private char[] _inputPhrases;
    private bool[] _revealedSlots;
    private int _inputCursorIndex;
    private int _cursorStartIndex;
    private int _cursorEndIndex;

    public bool IsDisplayInitailized
    {
        get
        {
            return _displayBoard.isDisplayInitialized;
        }
    }

    public int DisplayLineCount
    {
        get
        {
            return _displayBoard.LineCount;
        }
    }

    public bool PuzzleIsRevealed
    {
        get;
        set;
    }

    public int PickedLetterCount
    {
        get { return (_revealedCharString.Length - 6); }
    }

    public char RevealedVowel
    {
        get { return _revealedCharString[9]; }
    }

    public string RevealedConsonants
    {
        get
        {
            if (_revealedCharString.Length < 10)
            {
                return _revealedCharString.Substring(5);
            }
            else
            {
                return _revealedCharString.Substring(5, 3);
            }
        }
    }

    public bool IsFinishedInputRevealChar { get; set; }

    public bool IsFinishedInput { get; private set; }

    /// <summary>
    /// Use to set the phrase to newPhrase
    /// </summary>
    public void SetPhrase(PhraseObject phrase)
    {
        /*for (var i = 1; i <= _numOfRevealCharacter; i++)
        {
            tempRevealedCharText += "_";
        }*/
        
        // _hintPanelDisplay.SetRevealedCharText(tempRevealedCharText);
        _displayBoard.Init(phrase);
        _hintPanelDisplay.SetHintText(phrase.Hint);

        _phrase = phrase.Phrase.ToUpper();

        // init new array with a similar length as the phrase
        _inputPhrases = new char[_phrase.Length];
        _inputPhrases.Fill(' ');
        _revealedSlots = new bool[_phrase.Length];
        _revealedSlots.Fill(false);
        RevealPhrase(_splitLineCharacter);

        // init the attributes
        IsFinishedInputRevealChar = false;
        IsFinishedInput = false;
        _inputCursorIndex = 0;
        _cursorStartIndex = 0;
        _cursorEndIndex = _phrase.Length - 1;

        // reset the revealed string and reveal it
        _revealedCharString = _defaultRevealedCharString;
        if (_revealedCharString.Length > _numOfRevealCharacter)
        {
            Debug.LogError("[SetPhrase] _defaultRevealedCharString.length > _numOfRevealCharacter");
        }
        else
        {
            /*
            foreach (var character in _revealedCharString)
            {
                // RevealPhrase(character.ToString());
                // _hintPanelDisplay.AddRevealedCharText(character);
            }*/
        }

        // Reveal those other special characters
        RevealPhrase(" ");
        RevealPhrase("-");
        RevealPhrase("&");
        RevealPhrase("/");
        RevealPhrase("'");
        RevealPhrase(",");
        RevealPhrase(".");
        RevealPhrase("?");
        Debug.Log("Reveal " + _revealedCharString + "-&/',.?");


        //        _hintPanelDisplay.SetIntroText("Please pick " + (_numOfRevealCharacter - _revealedCharString.Length) + " more consonants that appear in the phrase above.");
        // hintPanelDisplay.SetIntroText("Please pick a vowel that appear in the phrase above");
        //_displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, false);
        //_displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, false);
    }

    public void FinishInputReveal()
    {
        IsFinishedInputRevealChar = true;
        _displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, true);
        // _displayBoard.LightEmptyAllLines();
        _displayBoard.DisplayAllLines();

        for (int i = 0; i < _revealedSlots.Length; i++)
        {
            IsFinishedInput = true;

            if (_revealedSlots[i] == false)
            {
                IsFinishedInput = false;
                break;
            }
        }

        // _hintPanelDisplay.SetIntroText("Guess the phrase by tapping the letter that goes in the highlighted square above.");
    }

    public void SetCursor()
    {
        _displayBoard.LightEmptyAllLines();
        _displayBoard.DisplayAllLines();
    }

    /// <summary>
    /// Use to manage the input from keyboard
    /// </summary>
    public void InputNewKey(KeyButton inputKey)
    {
        if (PuzzleIsRevealed)
        {
            if (!IsFinishedInputRevealChar)
            {

                if (inputKey.Key.Equals(_backspaceKeyValue))
                {
                    return;
                }

                _revealedCharString += inputKey.Key;
                StartCoroutine(inputKey.disableButton());
                if (_phrase.Contains(inputKey.Key))
                {
                    // inputKey.SetRightRevealButton();
                    // AudioSource.PlayClipAtPoint(_pingSound, new Vector3());
                }
                // RevealPhrase(inputKey.Key);
                // _hintPanelDisplay.AddRevealedCharText(inputKey.Key);


                // _hintPanelDisplay.SetIntroText("Please pick " + (_numOfRevealCharacter - _revealedCharString.Length) + " more consonants that appear in the phrase above.");
                if (_revealedCharString.Length == _numOfRevealCharacter)
                {
                    FinishInputReveal();
                }
            }
            else
            {
                if (inputKey.Key.Equals(_backspaceKeyValue))
                {
                    DeleteLastAlphabet();
                }
                else
                {
                    FillCurrentAlphabetSlot(inputKey.Key[0]);
                }
                _displayBoard.LightEmptyAllLines();
                _displayBoard.DisplayAllLines();
            }
        }
    }

    /// <summary>
    /// Use to display answer board
    /// </summary>
    public void Display()
    {
        _displayBoard.DisplayAllLines();
    }

    /// <summary>
    /// Use to undisplay answer board
    /// </summary>
    public void Undisplay()
    {
        _displayBoard.UndisplayAllLines();
    }

    /// <summary>
    /// Use to reset the input phrase
    /// </summary>
    public void ResetInputPhrase()
    {
        while (_inputCursorIndex > _cursorStartIndex)
        {
            DeleteLastAlphabet();
        }
        _displayBoard.LightEmptyAllLines(true);
        _displayBoard.DisplayAllLines();
    }

    /// <summary>
    /// Use to check the input answer is correct or not
    /// </summary>
    public bool CheckAnswer()
    {
        var answer = new string(_inputPhrases);

        if (_phrase.Equals(answer))
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Use to reveal the correct answer
    /// </summary>
    public void ShowAnswer()
    {
        _displayBoard.ShowAnswerInBoard();
    }

    /// <summary>
    /// Use to fill the input array at [inputCursor]
    /// </summary>
    private void FillCurrentAlphabetSlot(char inputChar)
    { 
        if (IsFinishedInput)
        {
            Debug.LogWarning("[FillCurrentAlphabetSlot] Cannot fill alphabet");
            return;
        }
        
        _inputPhrases[_inputCursorIndex] = inputChar;

        // Move inputCursor right to next non-revealed slot or end index
        do
        {
            _inputCursorIndex++;
            if (_inputCursorIndex > _cursorEndIndex)
            {
                Debug.Log("Finished input");
                _displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, false);
                IsFinishedInput = true;
                return;
            }
        } while (_revealedSlots[_inputCursorIndex]);
        _displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, true);
    }

    /// <summary>
    /// Use to delete last filled slot
    /// </summary>
    private void DeleteLastAlphabet()
    {
        if (_inputCursorIndex > _cursorStartIndex)
        {
            // Move inputCursor left to previous non-revealed slot or start index
            do
            {
                if (_inputCursorIndex > _cursorStartIndex)
                {
                    _inputCursorIndex--;
                }
            } while (_revealedSlots[_inputCursorIndex]);

            _inputPhrases[_inputCursorIndex] = ' ';
            IsFinishedInput = false;
            _displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, true);
        }
        else
        {
            Debug.LogWarning("[DeleteLastAlphabet] Cannot delete alphabet because don't have input alphabet");
        }
    }

    public IEnumerator RevealPhraseOverTime()
    {
        for (var i = 0; i < _phrase.Length; i++)
        {
            if (_defaultRevealedCharString.Contains(_phrase[i]))
            {
                //if(PlayPage)

                yield return new WaitForSeconds(0.4f);

                _inputPhrases[i] = _phrase[i];
                _revealedSlots[i] = true;

                _displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, true);
                AudioSource.PlayClipAtPoint(_pingSound, GameObject.Find("Main Camera").gameObject.transform.position);
                yield return null;
            }
            UpdateCursorStartPoint();
            UpdateCursorEndPoint();

            yield return null;
        }

        PuzzleIsRevealed = true;
    }

    public IEnumerator RevealSelectedLetterOverTime()
    {
        string tempString = _revealedCharString.Substring(6);
        Debug.Log(tempString);
        
        for (var i = 0; i < _phrase.Length; i++)
        {
            if (tempString.Contains(_phrase[i]))
            {
                yield return new WaitForSeconds(0.4f);
                
                _inputPhrases[i] = _phrase[i];
                _revealedSlots[i] = true;

                _displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, true);
                AudioSource.PlayClipAtPoint(_pingSound, GameObject.Find("Main Camera").gameObject.transform.position);
                yield return null;
            }
            UpdateCursorStartPoint();
            UpdateCursorEndPoint();

            yield return null;
        }

        PuzzleIsRevealed = true;

        yield return null;
    }



    /// <summary>
    /// Use to reveal a character in the display string
    /// </summary>
    private void RevealPhrase(string revealString)
    {
        var revealCharacter = revealString[0];
        
        if (_phrase.Contains(revealCharacter))
        {
            for (var i = 0; i < _phrase.Length; i++)
            {
                if (_phrase[i] == revealCharacter)
                {
                    _inputPhrases[i] = revealCharacter;
                    _revealedSlots[i] = true;
                }
            }
            UpdateCursorStartPoint();
            UpdateCursorEndPoint();
        }
        //_displayBoard.UpdateDisplayBoard(new string(_inputPhrases), _inputCursorIndex, false);
    }

    public void PlayHintAnimationOutThenIn(Action onComplete, string phrase)
    {
        PlayHintAnimationOut(() =>
            {
                _hintPanelDisplay.SetIntroText(phrase);
                PlayHintAnimationIn(onComplete);
            });
    }

    public void PlayHintAnimationInNonInvasive(Action onComplete, string phrase)
    {
        PlayHintAnimationOut(() =>
            {
                _hintPanelDisplay.SetIntroText(phrase);
                _hintPanelDisplay.DoHintAnimationInNonInvasive(onComplete);
            });
       

    }

    public void PlayHintAnimationIn(Action onComplete)
    {
        _hintPanelDisplay.DoHintAnimationIn(onComplete);
    }

    public void PlayHintAnimationOut(Action onComplete)
    {
        _hintPanelDisplay.DoHintAnimationOut(onComplete);
    }

    /// <summary>
    /// Use to update the cursor start point
    /// </summary>
    private void UpdateCursorStartPoint()
    {
        while (_revealedSlots[_cursorStartIndex])
        {
            if (_cursorStartIndex >= _cursorEndIndex)
            {
                break;
            }
            _cursorStartIndex++;
        }
        _inputCursorIndex = _cursorStartIndex;
    }

    /// <summary>
    /// Use to update the cursor start point
    /// </summary>
    private void UpdateCursorEndPoint()
    {
        while (_revealedSlots[_cursorEndIndex])
        {
            if (_cursorEndIndex <= _cursorStartIndex)
            {
                break;
            }
            _cursorEndIndex--;
        }
    }
}
