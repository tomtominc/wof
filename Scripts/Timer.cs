﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Text _timerText;
    [SerializeField] private List<Animator> _timerCells;

    private float _timeCounter;
    private int _startTime = 30;
    private int _winkIndex = -1;
    private bool _pasueGame = false;
    private bool _isStopped = false;

    public float timeCounter
    {
        get { return _timeCounter; }
        set
        {
            if (value > _startTime)
            {
                Debug.LogError("[UpdateTimer()] Can't set timeCounter because input time is more than timer's start time");
            }
            else
            {
                _timeCounter = value;
                UpdateTimer((int)Mathf.Ceil(_timeCounter));
            }
        }
    }

    public bool IsTimerStart { get; set; }

    public bool IsTimerRunning { get; set; }

    public bool IsTimeUp
    {
        get
        {

            if (timeCounter <= 0)
            {
                return true;
            }

            return false;
        }
    }

    /// <summary>
    /// Use to set disable timer display
    /// </summary>
    public void DisableTimerDisplay()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Use to set enable timer display
    /// </summary>
    public void EnableTimerDisplay()
    {
        gameObject.SetActive(true);
    }

    // Use this for initialization
    private void Start()
    {
        IsTimerStart = false;
        // _timeCounter = _startTime;
        _timerText.text = timeCounter.ToString();
    }

    // Update is called once per frame
    private void Update()
    {
        if (IsTimeUp || !IsTimerRunning || !IsTimerStart || _pasueGame)
            return;

        timeCounter -= Time.deltaTime;
        UpdateTimer((int)Mathf.Ceil(timeCounter));
        
    }

    public void ResetTimer(int time)
    {
        _timeCounter = time;
        _timerText.text = time.ToString();
    }

    public void StartCountDown()
    {
        if (!IsTimerStart)
        {
            IsTimerStart = true;
            IsTimerRunning = true;
            // IsTimeUp = false;
        }
    }

    public void StopCountDown()
    {
        if (IsTimerStart)
        {
            IsTimerStart = false;
            IsTimerRunning = false;
            // IsTimeUp = true;
        }
    }

    public void ResumeCountDown()
    {
        _pasueGame = false;
        if (IsTimerStart && !IsTimeUp)
        {
            IsTimerRunning = true;
        }
    }

    public void PauseCountDown()
    {
        _pasueGame = true;
        if (IsTimerStart && !IsTimeUp)
        {
            IsTimerRunning = false;
        }
    }

    /// <summary>
    /// Use to check TimeUp and update all TimerCells
    /// </summary>
    private void UpdateTimer(int time)
    {
        if (IsTimeUp)
        {
            _timerText.text = time.ToString();
            for (var i = _timerCells.Count / 2 + 1; i >= 0; i--)
            {
                _timerCells[i].Play("TimerRed3");
                _timerCells[_timerCells.Count - i - 1].Play("TimerRed3");
            }
            //Debug.LogError("[UpdateTimer()] Can't use this function because timer is end");
            return;
        }

        _timerText.text = time.ToString();
        var winkIndex = (int)Mathf.Floor((_startTime - time) * ((_timerCells.Count + 1) / 2.0f / _startTime));
        if (winkIndex == (_timerCells.Count + 1) / 2)
        {
            // IsTimeUp = true;
            for (var i = _timerCells.Count / 2 + 1; i >= 0; i--)
            {
                _timerCells[i].Play("TimerRed3");
                _timerCells[_timerCells.Count - i - 1].Play("TimerRed3");
            }
        }
        else if (_winkIndex != winkIndex && winkIndex < _timerCells.Count / 2.0f)
        {
            _winkIndex = winkIndex;
            // IsTimeUp = false;
            for (var i = _timerCells.Count / 2 + 1; i >= 0; i--)
            {
                if (i > winkIndex)
                {
                    _timerCells[i].Play("TimerIdle");
                    _timerCells[_timerCells.Count - i - 1].Play("TimerIdle");
                }
                else if (i == winkIndex)
                {
                    _timerCells[i].Play("TimerWink");
                    _timerCells[_timerCells.Count - i - 1].Play("TimerWink");
                }
                else if (i == winkIndex - 1)
                {
                    _timerCells[i].Play("TimerRed1");
                    _timerCells[_timerCells.Count - i - 1].Play("TimerRed1");
                }
                else if (i == winkIndex - 2)
                {
                    _timerCells[i].Play("TimerRed2");
                    _timerCells[_timerCells.Count - i - 1].Play("TimerRed2");
                }
                else
                {
                    _timerCells[i].Play("TimerRed3");
                    _timerCells[_timerCells.Count - i - 1].Play("TimerRed3");
                }
            }
        }
    }
}
