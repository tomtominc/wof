﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitlePage : MonoBehaviour
{
    private const string _animatorStopStateName = "EmptyState";
    private const string _animatorEndStateName = "EndState";
    [SerializeField] private Animator _titlePageAnimator;
    [SerializeField] private Image _playButtonImage;
    
    [SerializeField] private AudioClip _wofChantSound;

    [SerializeField] private GameObject _hiddenTimerPrefab;
    public HiddenTimer hTimer;

    private float chantTimer;

    private TitlePageState _titlePageState;
    //    private float _timeToExitToShell = 30.0f;
    private enum TitlePageState
    {
        animatePageIn,
        shownPage,
        animatePageOut,
        endPage
    }

    public bool IsTitlePageEnd { get; private set; }
    
    // Use to end title page with fade out animation
    public void EndTitlePage()
    {
        _titlePageAnimator.SetBool("Out", true);
        IsTitlePageEnd = true;
        _titlePageState = TitlePageState.animatePageOut;
    }

    // Use this for initialization
    private void Start()
    {
        chantTimer = 3.5f;
        _titlePageState = TitlePageState.animatePageIn;
    }

    // Update is called once per frame
    private void Update()
    {
        if (_titlePageState == TitlePageState.animatePageIn && chantTimer > 0)
        {
            chantTimer -= Time.deltaTime;
            //if (_titlePageAnimator.GetCurrentAnimatorStateInfo(0).IsName("TitlePageIn"))// && !isChantPlayed)
            // {
            // isChantPlayed = true;
            if (chantTimer <= 0)
            {
                AudioSource.PlayClipAtPoint(_wofChantSound, GameObject.Find("Main Camera").gameObject.transform.position);
            }
            //  }
        }
        if (_titlePageState == TitlePageState.animatePageIn && _titlePageAnimator.GetCurrentAnimatorStateInfo(0).IsName(_animatorStopStateName) && chantTimer <= 0)
        {

            _titlePageState = TitlePageState.shownPage;
            
            // Setup Timer
            hTimer = Instantiate(_hiddenTimerPrefab).GetComponent<HiddenTimer>();
            hTimer.transform.SetParent(this.transform, false);
            hTimer.Init(15);

        }
        if (_titlePageState == TitlePageState.shownPage)
        {
            if (hTimer.IsTimeUp)
            {
                EndTitlePage();
            }
        }
        if (_titlePageState == TitlePageState.animatePageOut && _titlePageAnimator.GetCurrentAnimatorStateInfo(0).IsName(_animatorEndStateName))
        {
            _titlePageState = TitlePageState.endPage;
            Destroy(gameObject);
        }
    }
}
