﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using DG.Tweening;

public class WheelPage : MonoBehaviour
{

    private const float _minimunAngularVelocity = 250f;

    [SerializeField] private AudioClip _wheelSpinSound;
    [SerializeField] private AudioClip _fireworksSound;
    [SerializeField] private AudioClip _crowdSound;

    [SerializeField] private WheelScript _wheel;
    [SerializeField] private RectTransform _congratsText;
    [SerializeField] private Animator _spinButtonCanvas;
    [SerializeField] private Animator _suggestionArrowAnimator;
    [SerializeField] private GameObject _congratEffectPrefab;
    
    [SerializeField] private GameObject _hiddenTimerPrefab;
    public HiddenTimer hTimer;
    private Transform _wheelTransform;
    private Vector3 _wheelInitPosition;
    private GameObject _congratEffect;

    private bool _isClicked;
    private Vector2 _oldMousePosition;
    private Vector2 _newMousePosition;
    private float _angularDistance;
    private float _dragTime;
    private float _z;

    public bool SignForShowReward { get; private set; }

    public bool IsWheelPageEnd { get; private set; }

    public bool IsRewardPieShown { get; private set; }

    private bool _isTextRewardShown;

    public void SpinWheelAuto()
    {
        _wheel.SetSpinWheel(Random.Range(-601, -250), 2.0f);
        _suggestionArrowAnimator.SetBool("Animate", false);

//		AudioSource _au = PlayClipAt(_wheelSpinSound, GameObject.Find("Main Camera").gameObject.transform.position);
//		_au.pitch = 0.75f;
    }

    public void SpinWheelAutoCCW()
    {
        _wheel.SetSpinWheel(Random.Range(250, 601), 3.0f);
        _suggestionArrowAnimator.SetBool("Animate", false);

//		AudioSource _au = PlayClipAt(_wheelSpinSound, GameObject.Find("Main Camera").gameObject.transform.position);
//		_au.pitch = 0.75f;
    }

    public AudioSource PlayClipAt(AudioClip clip, Vector3 pos)
    {
        GameObject tempGO = new GameObject("TempAudio"); 
        tempGO.transform.position = pos; 
        AudioSource aSource = tempGO.AddComponent<AudioSource>();
        aSource.clip = clip; 
        aSource.Play(); 
        Destroy(tempGO, clip.length);
        return aSource; 
    }

    // Use this for initialization
    private void Start()
    {
        _isTextRewardShown = false;
        SignForShowReward = false;
        IsRewardPieShown = false;
        IsWheelPageEnd = false;
        _wheelInitPosition = _wheel.transform.position;

        _wheelTransform = _wheel.gameObject.GetComponent<Transform>();
        _congratsText.gameObject.transform.parent.gameObject.SetActive(false);

        _wheel.SetupWheelWithRewardItems(FullGameScene.Instance.RewardItems);
        _wheel.InitWheel();

        // Setup Timer
        hTimer = Instantiate(_hiddenTimerPrefab).GetComponent<HiddenTimer>();
        hTimer.transform.SetParent(this.transform, false);
        hTimer.Init(15);

    }

    // Update is called once per frame
    private void Update()
    {

        if (_wheel.WheelState == WheelScript.WheelStates.noSpin)
        {
            
            // allow player to spin while time is not up
            if (!hTimer.IsTimeUp)
            {
                // Spin logic
                if (Input.GetMouseButton(0) && !_isClicked)
                {
                    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                    if (hit.collider != null && hit.collider.gameObject == _wheel.gameObject)
                    {
                        _suggestionArrowAnimator.SetBool("Animate", false);
                        _isClicked = true;
                        _z = 0;
                        _dragTime = 0;
                        _angularDistance = 0;

                        _newMousePosition =
	                        new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - _wheelTransform.position.x,
                            Camera.main.ScreenToWorldPoint(Input.mousePosition).y - _wheelTransform.position.y)
	                            .normalized;

                    }
                }
                else if (Input.GetMouseButton(0) && _isClicked)
                {
                    _wheel.transform.position = _wheelInitPosition;
                    _oldMousePosition = _newMousePosition;
                    _newMousePosition =
	                    new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - _wheelTransform.position.x,
                        Camera.main.ScreenToWorldPoint(Input.mousePosition).y - _wheelTransform.position.y).normalized;

                    if (_z != Vector3.Cross(_oldMousePosition, _newMousePosition).normalized.z)
                    {
                        _z = Vector3.Cross(_oldMousePosition, _newMousePosition).normalized.z;
                        _dragTime = 0;
                        _angularDistance = 0;
                    }
                    else
                    {
                        float angleChange =
                            Mathf.Acos(Vector2.Dot(_oldMousePosition, _newMousePosition) /
                                (_oldMousePosition.magnitude * _newMousePosition.magnitude));
                        if (!float.IsNaN(angleChange))
                        {
                            _wheelTransform.eulerAngles += new Vector3(0, 0, _z * angleChange * 180 / Mathf.PI);

                            _dragTime += Time.deltaTime;
                            _angularDistance += _z * angleChange * 180 / Mathf.PI;
                        }

                    }
                }
                else if (!Input.GetMouseButton(0) && _isClicked)
                {
                    _isClicked = false;
                    if (_z != 0)
                    {
                        if (_dragTime != 0)
                        {
                            if (Mathf.Abs(_angularDistance / _dragTime) < _minimunAngularVelocity)
                            {
                                Debug.LogWarning("[Wheel too slow] angularVelocity = " + _angularDistance / _dragTime + " < " +
                                    _minimunAngularVelocity + " , Please spin wheel again");
                                if (_z > 0)
                                    SpinWheelAutoCCW();
                                else
                                    SpinWheelAuto();
                            }
                            else
                            {
                                _wheel.SetSpinWheel(_angularDistance / _dragTime, 3.0f);
                                Debug.Log("angularVelocity = " + _angularDistance / _dragTime);

                                AudioSource.PlayClipAtPoint(_wheelSpinSound, GameObject.Find("Main Camera").gameObject.transform.position);

                            }
                        }
                        else
                        {
                            if (_z > 0)
                                SpinWheelAutoCCW();
                            else
                                SpinWheelAuto();
                        }
                    }
                    else
                    {
                        SpinWheelAuto();
                    }
                    Debug.Log("z : " + _z + " // _dragTime : " + _dragTime);
                    
                }


            }
            else
            {
                // Auto spin 
                Debug.Log("Time's up : Auto Spin");
                // move to another state
                SpinWheelAuto();

            }

	        
        }
        else if (_wheel.WheelState == WheelScript.WheelStates.spinning)
        {
            //_spinButtonCanvas.gameObject.SetActive(false);
            _spinButtonCanvas.SetBool("out", true);
            hTimer.PauseTimer();
        }
        else if (_wheel.WheelState == WheelScript.WheelStates.spined)
        {
           

            if (!SignForShowReward)
            {
               
               
                IsRewardPieShown = true;
                StartCoroutine(WaitForSecondsAndDoFunction(2.0f,
                        () =>
                        {
                            _wheel.PlayRewardPieSFX();
                            gameObject.GetComponent<Animator>().enabled = true;
                            gameObject.GetComponent<Animator>().SetBool("Zoom", true);
                            StartCoroutine(WaitForSecondsAndDoFunction(1.0f, () =>
                                    {
                                        _wheel.ShowRewardPie();
                                        _congratEffect = Instantiate(_congratEffectPrefab);
                                        _congratEffect.transform.SetParent(gameObject.transform, false);
                                        _congratsText.gameObject.SetActive(true);
                                        
                                        _congratsText.DOScale(new Vector3(0f, 0f, 0f), 0.5f).SetEase(Ease.OutBack).From();
                                        
                                    }));
                        }
                    ));


                SignForShowReward = true;
            }
        }
        else if (_wheel.WheelState == WheelScript.WheelStates.showedRewardPie && !_isTextRewardShown)
        {
            _isTextRewardShown = true;
            //StartCoroutine(_wheel.TurnWedgeAround());
            _wheel.TurnWedge();

            AudioSource.PlayClipAtPoint(_crowdSound, GameObject.Find("Main Camera").gameObject.transform.position);
            AudioSource.PlayClipAtPoint(_fireworksSound, GameObject.Find("Main Camera").gameObject.transform.position);
        }
        else if (_isTextRewardShown)
        {
            StartCoroutine(WaitForSecondsAndDoFunction(6.0f, () =>
                    {
                        IsWheelPageEnd = true;
                    }));
        }
    }


    private IEnumerator WaitForSecondsAndDoFunction(float waitTime, Action actionFunction)
    {
        yield return new WaitForSeconds(waitTime);
        actionFunction();
    }
}
