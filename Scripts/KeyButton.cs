﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class KeyButton : MonoBehaviour, IPointerClickHandler, IPointerDownHandler,IPointerUpHandler
{
    public Button Button;
    public bool changeColor = true;
    [SerializeField] private Sprite _rightButtonSprite;
    [SerializeField] private Text _keyText;


    private Tweener _scaleTween;
    private Color disableColor = new Color(0.6f, 0.6f, 0.6f, 1f);

    public event Action OnDisabledClickAction = delegate { };
    
    // RGB : 50 50 50
    // private Color32 blackKey = new Color32(50, 50, 50, 255);
    // RGB : 209 209 209
    // private Color32 greyKey = new Color32(209, 209, 209, 255);

    private bool _isAbleToPressed;

    public string Key { get; private set; }

    public bool IsAbleToPressed
    {
        get
        {
            return _isAbleToPressed;
        }
        set
        {
            _isAbleToPressed = value;
        }
    }

    // Use this for initialization
    private void Awake()
    {
        Key = name;
        Button = GetComponent<Button>();
        _keyText = GetComponentInChildren<Text>();

        if (_keyText != null && changeColor)
        {
            // Debug.Log(_keyText.text + " " + _keyText.color);

            if (Button.interactable)
            {
                _keyText.color = new Color32(50, 50, 50, 255);
                ;
            }
            else
            {
                _keyText.color = new Color32(209, 209, 209, 255);
            }
        }
    }

    public void OnPointerClick(PointerEventData evd)
    {
        // Debug.Log(Key + " : " + Button.interactable);

        if (!Button.interactable)
        {
            // Instantiate();
            OnDisabledClickAction();
        }

    }

    public void OnPointerDown(PointerEventData e)
    {
        if (Button.interactable)
        {
            transform.localScale = new Vector3(0.95f, 0.95f, 0.95f);
        }
    }

    public void OnPointerUp(PointerEventData e)
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
    }


    // Use to set sprite this button to right Reveal
    public void SetRightRevealButton()
    {
        var tempSpriteState = new SpriteState();
        tempSpriteState.pressedSprite = Button.spriteState.pressedSprite;
        tempSpriteState.highlightedSprite = Button.spriteState.highlightedSprite;
        tempSpriteState.disabledSprite = _rightButtonSprite;
        Button.spriteState = tempSpriteState;
    }

    // Use to disable this key button
    public IEnumerator disableButton()
    {
        yield return new WaitForEndOfFrame();

        Button.interactable = false;

        if (changeColor)
            GetComponent < Image >().color = disableColor;
        
        if (_keyText != null && changeColor)
        {
            _keyText.color = disableColor;
        }
    }

    // Use to enable this key button
    public IEnumerator enableButton()
    {
        yield return new WaitForEndOfFrame();

        Button.interactable = true;

        if (changeColor)
            GetComponent < Image >().color = new Color(1f, 1f, 1f, 1f);
        if (_keyText != null && changeColor)
        {
            _keyText.color = new Color(1f, 1f, 1f, 1f);
        }
        
    }
}
