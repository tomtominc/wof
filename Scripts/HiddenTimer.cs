﻿using UnityEngine;
using System.Collections;

public class HiddenTimer : MonoBehaviour
{
    // public int StartingTime;
    public float TimeCounter;

    public bool IsTimerStart { get; private set; }

    public bool IsTimerRunning { get; private set; }

    public bool IsTimeUp { get; private set; }

    private void Update()
    {
        CountdownTimer();
    }

    public void Init(int startTime)
    {
        // StartingTime = startTime;
        TimeCounter = startTime;

        // Debug.Log("Init timer "+startTime+" seconds");

        IsTimerStart = true;
        IsTimerRunning = true;
        IsTimeUp = false;
    }

    public void PauseTimer()
    {
        if (IsTimerStart)
        {
            IsTimerRunning = false;
        }
    }

    public void ResumeTimer()
    {
        if (IsTimerStart)
        {
            IsTimerRunning = true;
        }
    }

    public void CountdownTimer()
    {
        if (IsTimerStart && IsTimerRunning && !IsTimeUp)
        {
            TimeCounter -= Time.deltaTime;
            
            if (TimeCounter <= 0)
            {
                Debug.Log("Time's up");
                IsTimeUp = true;
            }
        }
    }

}
