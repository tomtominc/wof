{
	"Date":"2016-05-23",
	"TextInPie":["", "700" , "" ,"650" ,"", "600", "500" , "550" ,"600" ,"500", "700", "500" , "650" ,"600" ,"700", "600", "500" , "" ,"" ,"900", "500", "650" , "500" ,"800" ],
	"Angle":[15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15],
	"PieColor":[ "LoseATurn", "Blue", "GrandPrize", "Lavender", "Bankrupt", "Pink", "Green", "Blue", "Red", "Pink", "Yellow", "Lavender", "Orange", "Blue", "Red", "Yellow", "Green", "GrandPrize", "Bankrupt", "Orange", "Green", "Pink", "Lavender", "Red"],
	"Probability":[0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1]
}
